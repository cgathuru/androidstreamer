# README #

# Android Streamer #

## Summary ##

Android Streamer is a music application meant to change the way people listen to their music, combing everything that people love about their music, and having all aspects of it integrated into a single App. It allows you to wirelessly share your music library with others on the go. In incorporates both your local and online libraries as well as music streaming services. Truly a one stop shop for your audio needs.