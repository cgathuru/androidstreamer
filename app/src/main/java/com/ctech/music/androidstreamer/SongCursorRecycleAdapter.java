package com.ctech.music.androidstreamer;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore.Audio.Media;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;

import wseemann.media.FFmpegMediaMetadataRetriever;

/**
 * Created by EGATCHA on 3/18/2015.
 * This Custom Adapter is for Songs
 */
 class SongCursorRecycleAdapter extends CursorRecycleAdapter<SongHolder> {

    private static final String LOGTAG = "SongRecycleAdapter";
    private Context mContext;

    public SongCursorRecycleAdapter(Context context, Cursor cursor) {
        super(cursor);
        this.mContext = context;
    }

    @Override
    public void onBindViewHolderCursor(SongHolder holder, Cursor cursor) {

        String data = cursor.getString(cursor.getColumnIndex(Media.DATA));
        File recheckSizeFile = new File(data);
        String title = cursor.getString(cursor.getColumnIndex(Media.TITLE));
        String artist = cursor.getString(cursor.getColumnIndex(Media.ARTIST));
        // Make sure the cursor has valid data
        try {


            Log.d(LOGTAG, "Now extracting metadata for " + data);
            FFmpegMediaMetadataRetriever ffmr = new FFmpegMediaMetadataRetriever();
            ffmr.setDataSource(data);

            title = ffmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_TITLE);
            if (title == null) {
                Log.e(LOGTAG, "Empty title " + title);
                title = cursor.getString(cursor.getColumnIndex(Media.TITLE));
            }

            artist = ffmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ARTIST);
            if (artist == null) {
                Log.e(LOGTAG, "Empty subtitle " + artist);
                artist = cursor.getString(cursor.getColumnIndex(Media.ARTIST));
            }


            /*byte[] picture = ffmr.getEmbeddedPicture();
            if (picture != null) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(picture, 0, picture.length);
                double bytes = bitmap.getByteCount();
                picture = null;
                Log.d(LOGTAG, "Album art is " + bytes/1000 + "KB");
                //holder.albumArt.setImageBitmap(bitmap);
            } else {
                //holder.albumArt.setImageResource(R.drawable.ic_action_name);
            }*/
        }
        catch (IllegalArgumentException e){
            e.printStackTrace();
            Toast.makeText(mContext,"The song "+ data + " refers to an incomplete or damaged file. Please check the song",Toast.LENGTH_LONG).show();
        }
        long albumId = cursor.getLong(cursor.getColumnIndex(Media.ALBUM_ID));
        Uri songCover = Uri.parse("content://media/external/audio/albumart");
        Uri uriSongCover = ContentUris.withAppendedId(songCover, albumId);
        Picasso.with(mContext).load(uriSongCover).fit().into(holder.albumArt);
        holder.title.setText(title);
        holder.subtitle.setText(artist);


    }

    @Override
    public SongHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.library_songs_item,parent,false);
        return new SongHolder(view);
    }
}