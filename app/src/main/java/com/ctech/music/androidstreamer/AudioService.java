package com.ctech.music.androidstreamer;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.session.MediaSessionManager;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

/**
 * Created by Charles on 21/03/2015.
 */
public class AudioService extends Service {

    private static final String LOGTAG = "AudioService";

    // These parameters represent the actions that can be performed by the media player
    public static final String ACTION_PLAY = "com.ctech.music.androidstreamer.PLAY";
    public static final String ACTION_PAUSE = "com.ctech.music.androidstreamer.PAUSE";
    public static final String ACTION_STOP = "com.ctech.music.androidstreamer.STOP";
    public static final String ACTION_NEXT = "com.ctech.music.androidstreamer.NEXT";
    public static final String ACTION_PREVIOUS = "com.ctech.music.androidstreamer.PREVIOUS";

    public static final String ACTION = "action";
    public static final String SERVICECMD = "serviceCmd";

    public static final String CMD_PLAY = "play";
    public static final String CMD_PAUSE = "pause";
    public static final String CMD_STOP = "stop";
    public static final String CMD_NEXT = "next";
    public static final String CMD_PREVIOUS = "previous";
    public static final String CMD_REWIND = "rewind";
    public static final String CMD_FAST_FORWARD = "fastForward";

    public static final String METADATA_CHANGED = "com.ctech.music.androidstreamer.metadataChanged";

    // These parameters manage the audio players state
    // MediaPlayerState Handler messages
    public static final int PLAYER_STATE_IDLE = 0;
    public static final int PLAYER_STATE_PREPARED = 1;
    public static final int PLAYER_STATE_STARTED = 2;
    public static final int PLAYER_STATE_PAUSED = 3;
    public static final int PLAYER_STATE_STOPPED = 4;
    public static final int PLAYER_STATE_COMPLETE = 5;
    public static final int PLAYER_STATE_END = 6;
    public static final int PLAYER_STATE_ERROR = 7;
    private int mPlayerState = PLAYER_STATE_END;

    // These parameters manage the Media Player environment
    // MediaPlayer Handler messages
    private static final int TRACK_ENDED = 0;
    private static final int TRACK_WENT_TO_NEXT = 1;
    private static final int RELEASE_WAKELOCK = 2;
    private static final int SERVER_DIED = 3;
    private static final int FADE_IN = 4;
    private static final int FADE_OUT = 5;
    private static final int FOCUS_CHANGE = 6;

    // The parameters indicate the default tasks that this handler manages
    // Task Handler messages
    private static final int PLAYLIST_CHANGED = 0;
    private static final int POSITION_CHANGED = 1;
    private static final int ADD_TO_HISTORY = 2;

    // These parameters are for the player handler to handle events received via broadcast

    public static final int NEXT = 0;
    public static final int PREVIOUS = 1;
    public static final int PLAY = 2;
    public static final int PAUSE = 3;
    public static final int ADD_SONG = 4;

    private MultiPlayer mPlayer;
    private LocalBroadcastManager bManager;
    private AudioManager mAudioManager;
    private MediaSessionManager mManager;
    private MediaSessionCompat mSession;
    private MediaControllerCompat mController;
    private PowerManager.WakeLock mWakeLock;

    private static boolean mIsSupposedToBePlaying = false;
    private int mPlayPos;
    private int mNextPlayPos;
    private Cursor mCursor;
    private Cursor mNextCursor;
    private Cursor mNextPlaylistCursor;
    private ArrayList<Integer> mPlayList;

    public static final int REPEAT_NONE = 0;
    public static final int REPEAT_CURRENT = 1;
    public static final int REPEAT_ALL = 2;
    private int mRepeatMode = REPEAT_NONE;

    public static final int SHUFFLE_NONE = 0;
    public static final int SHUFFLE_AUTO = 1;
    private int mShuffleMode = SHUFFLE_NONE;

    private boolean mPausedByTransientLossOfFocus = false;
    private boolean mServiceBound = false;
    private int mServiceStartId;
    private static final long IDLE_DELAY = 60000;

    public static final int PLAYLIST_SONGS = 0;
    public static final int PLAYLIST_ARTIST = 1;
    public static final int PLAYLIST_ALBUM = 2;
    public static final int PLAYLIST_GENRE = 3;
    private int mCurrentPlaylist = PLAYLIST_SONGS;
    private String mPlaylistArgs = null;

    // Constants to improve code legibility
    private static final int NONE = -1;
    private static final int ERROR = NONE;
    private static final int FINISHED = NONE;

    private int mOpenFailedCount = 0;

    private boolean mIsPlaylist = true;

    private final List<Integer> mSongHistory = new ArrayList<>();
    private int mHistoryPos = NONE;
    private boolean mAddToHistory = true;

    private final Object playerLock = new Object();
    private final Object cursorLock =  new Object();
    private final Song mCurrentSong = new Song();


    @Override
    public void onCreate() {
        //TODO
        super.onCreate();
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        HandlerThread taskThread = new HandlerThread("taskThread", android.os.Process.THREAD_PRIORITY_BACKGROUND);
        taskThread.start();
        HandlerThread playerThread = new HandlerThread("playerThread", android.os.Process.THREAD_PRIORITY_BACKGROUND);
        playerThread.start();
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock =  powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, this.getClass().getName());
        mWakeLock.setReferenceCounted(false);
        mTaskHandler = new TaskHandler(taskThread.getLooper());
        mBroadcastHandler =  new BroadcastHandler(playerThread.getLooper());
        mDelayedStopHandler.sendMessageDelayed(mDelayedStopHandler.obtainMessage(TRACK_ENDED),IDLE_DELAY);
        mTaskHandler.sendEmptyMessage(PLAYLIST_CHANGED);
        mPlayer = new MultiPlayer();
        mPlayer.setHandler(mMediaPlayerHandler);
        notifyChange(METADATA_CHANGED);
        bManager = LocalBroadcastManager.getInstance(getApplicationContext());
        IntentFilter commandFilter = new IntentFilter();
        commandFilter.addAction(AudioService.ACTION_NEXT);
        commandFilter.addAction(AudioService.ACTION_PREVIOUS);
        commandFilter.addAction(AudioService.ACTION_PAUSE);
        commandFilter.addAction(AudioService.ACTION_PLAY);
        commandFilter.addAction(MusicPlayerFragment.ADD_SONG);
        bManager.registerReceiver(mReceiver, commandFilter);
        mManager = (MediaSessionManager) getSystemService(Context.MEDIA_SESSION_SERVICE);
        //if (mManager != null)
            //initMediaSessions();
        mDelayedStopHandler.sendMessageDelayed(mDelayedStopHandler.obtainMessage(), IDLE_DELAY);
    }


    private void initMediaSessions(){
        ComponentName rec = new ComponentName(getPackageName(),
                MediaButtonReceiver.class.getName());
        Intent i = new Intent(Intent.ACTION_MEDIA_BUTTON);
        i.setComponent(rec);
        PendingIntent pi = PendingIntent.getBroadcast(this /*context*/,
                0 /*requestCode, ignored*/, i /*intent*/, 0 /*flags*/);
       /// mSession = new MediaSessionCompat(getApplicationContext(), "simple player session");
        mSession =  new MediaSessionCompat(getApplicationContext(),"MediaSession",rec,pi);
       mController = mSession.getController();
        mSession.setFlags(
                MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        // TODO Set the correct callback
        /*mSession.setCallback(new MediaSessionCompat.Callback(){
             @Override
             public void onPlay() {
                 super.onPlay();
                 Log.e( "MediaPlayerService", "onPlay");
                 buildNotification( generateAction( android.R.drawable.ic_media_pause, "Pause", ACTION_PAUSE ) );
             }

             @Override
             public void onPause() {
                 super.onPause();
                 Log.e( "MediaPlayerService", "onPause");
                 buildNotification(generateAction(android.R.drawable.ic_media_play, "Play", ACTION_PLAY));
             }

             @Override
             public void onSkipToNext() {
                 super.onSkipToNext();
                 Log.e( "MediaPlayerService", "onSkipToNext");
                 //Change media here
                 buildNotification( generateAction( android.R.drawable.ic_media_pause, "Pause", ACTION_PAUSE ) );
             }

             @Override
             public void onSkipToPrevious() {
                 super.onSkipToPrevious();
                 Log.e( "MediaPlayerService", "onSkipToPrevious");
                 //Change media here
                 buildNotification( generateAction( android.R.drawable.ic_media_pause, "Pause", ACTION_PAUSE ) );
             }

             @Override
             public void onFastForward() {
                 super.onFastForward();
                 Log.e( "MediaPlayerService", "onFastForward");
                 //Manipulate current media here
             }

             @Override
             public void onRewind() {
                 super.onRewind();
                 Log.e( "MediaPlayerService", "onRewind");
                 //Manipulate current media here
             }

             @Override
             public void onStop() {
                 super.onStop();
                 Log.e( "MediaPlayerService", "onStop");
                 //Stop media player here
                 NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                 notificationManager.cancel( 1 );
                 Intent intent = new Intent( getApplicationContext(), AudioService.class );
                 stopService( intent );
             }

             @Override
             public void onSeekTo(long pos) {
                 super.onSeekTo(pos);
             }

             //@Override
             public void onSetRating(Rating rating) {
                 //super.onSetRating(rating);
             }
         }
        );*/
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //TODO Complete start commnad
        mServiceStartId = startId;
        mDelayedStopHandler.removeCallbacksAndMessages(null);
        if (intent == null){
            Log.e(LOGTAG, "The intent is null");
        }
        /*if (intent != null) {
            String action = intent.getAction();
            String cmd = intent.getStringExtra("command");
            if (CMD_NEXT.equals(cmd) || action.equals(ACTION_NEXT)) {
                goToNext(true);
            } else if (CMD_PREVIOUS.equals(cmd) || action.equals(ACTION_PREVIOUS)) {
                if (getPosition() < 2000) {
                    previous();
                } else {
                    synchronized (playerLock){
                        seekTo(0);
                    }
                    play();
                }
            } else if (CMD_PAUSE.equals(cmd) || action.equals(ACTION_PAUSE)) {
                pause();
                mPausedByTransientLossOfFocus = false;
            } else if (CMD_PLAY.equals(cmd)) {
                play();
            } else if (CMD_STOP.equals(cmd)) {
                pause();
                mPausedByTransientLossOfFocus = false;
                synchronized (playerLock){
                    seekTo(0);
                }
            }
        }*/

        // make sure the service will shut down on its own if it was
        // just started but not bound to and nothing is playing
        mDelayedStopHandler.removeCallbacksAndMessages(null);
        Message msg = mDelayedStopHandler.obtainMessage();
        mDelayedStopHandler.sendMessageDelayed(msg, IDLE_DELAY);
        return START_STICKY;
    }

    private Notification.Action generateAction( int icon, String title, String intentAction ) {
        Intent intent = new Intent( getApplicationContext(), AudioService.class );
        intent.setAction( intentAction );
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        //return new Notification.Action.Builder( icon, title, pendingIntent ).build();
        return null;
    }

   /* private void buildNotification( Notification.Action action ) {
        Notification.MediaStyle style = new Notification.MediaStyle();

        Intent intent = new Intent( getApplicationContext(), AudioService.class );
        intent.setAction( ACTION_STOP );
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        Notification.Builder builder = new Notification.Builder( this )
                .setSmallIcon(R.drawable.ic_action_name)
                .setContentTitle( "Media Title" )
                .setContentText( "Media Artist" )
                .setDeleteIntent( pendingIntent )
                .setStyle(style);

        builder.addAction( generateAction( android.R.drawable.ic_media_previous, "Previous", ACTION_PREVIOUS ) );
        builder.addAction( generateAction( android.R.drawable.ic_media_rew, "Rewind", CMD_REWIND ) );
        builder.addAction( action );
        builder.addAction( generateAction( android.R.drawable.ic_media_ff, "Fast Foward", CMD_FAST_FORWARD ) );
        builder.addAction( generateAction( android.R.drawable.ic_media_next, "Next", ACTION_NEXT ) );
        style.setShowActionsInCompactView(0,1,2,3,4);

        NotificationManager notificationManager = (NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );
        notificationManager.notify( 1, builder.build() );
    }*/

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // Determine what to do on each other the actions
            if (action.equals(AudioService.ACTION_PLAY)) {
                if (!mIsSupposedToBePlaying)
                    mBroadcastHandler.sendEmptyMessage(PLAY);
            } else if (action.equals(AudioService.ACTION_PAUSE)) {
                if (isPlaying()) {
                    mBroadcastHandler.sendEmptyMessage(PAUSE);
                }
            } else if (action.equals(AudioService.ACTION_NEXT)) {
                mBroadcastHandler.sendEmptyMessage(NEXT);
            } else if (action.equals(AudioService.ACTION_PREVIOUS)) {
                mBroadcastHandler.sendEmptyMessage(PREVIOUS);
            } else if (action.equals(MusicPlayerFragment.ADD_SONG)){
                Log.d(LOGTAG, "Received request to add song");
                Song newSong = intent.getParcelableExtra(Constants.SONG);
                int currentPlaylist = intent.getIntExtra(Constants.CURRENT_PLAYLIST, PLAYLIST_SONGS);
                int newPosition = intent.getIntExtra(Constants.POSITION,0);
                Log.d(LOGTAG,"Current position is "+ mPlayPos + " and the next position is " + newPosition);
                setCurrentSong(newSong);
                //mTaskHandler.obtainMessage(POSITION_CHANGED,newPosition,0).sendToTarget();
                setPlayPos(newPosition);
                if (mCurrentPlaylist != currentPlaylist){
                    setCurrentPlaylist(currentPlaylist);
                    mTaskHandler.obtainMessage(PLAYLIST_CHANGED).sendToTarget();
                }
                //goToNext(false);
                mBroadcastHandler.sendEmptyMessage(ADD_SONG);
            }
            /*else if( action.equalsIgnoreCase( CMD_PLAY ) ) {
                mController.getTransportControls().play();
            } else if( action.equalsIgnoreCase( CMD_PAUSE ) ) {
                mController.getTransportControls().pause();
            } else if( action.equalsIgnoreCase( CMD_FAST_FORWARD ) ) {
                mController.getTransportControls().fastForward();
            } else if( action.equalsIgnoreCase( CMD_REWIND ) ) {
                mController.getTransportControls().rewind();
            } else if( action.equalsIgnoreCase( CMD_PREVIOUS ) ) {
                mController.getTransportControls().skipToPrevious();
            } else if( action.equalsIgnoreCase( CMD_PREVIOUS ) ) {
                mController.getTransportControls().skipToNext();
            } else if( action.equalsIgnoreCase( CMD_STOP ) ) {
                mController.getTransportControls().stop();
            }*/

            //TODO Add and remove song
        }
    };

    private void setCurrentSong(Song newSong) {
        synchronized (this){
            mCurrentSong.id = newSong.id;
            mCurrentSong.data = null;
            mCurrentSong.albumId = NONE;
            mCurrentSong.artist = null;
        }
    }


    private void updateSongList(){
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        String order = MediaStore.Audio.Media.TITLE + " ASC";
        String[] args = null;
        String[] projection = {
                MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.ALBUM_ID
        };
        Cursor cursor;
        switch (mCurrentPlaylist){
            case PLAYLIST_SONGS:
                break;
            case PLAYLIST_ARTIST:
                selection = MediaStore.Audio.Media.ARTIST + " =?";
                args = new String[]{mPlaylistArgs};
                break;
            case PLAYLIST_ALBUM:
                selection = MediaStore.Audio.Media.ALBUM + " =?";
                args = new String[]{mPlaylistArgs};
                break;
            case PLAYLIST_GENRE:
                uri = MediaStore.Audio.Genres.Members.getContentUri("external", Integer.parseInt(mPlaylistArgs));
                break;
        }
        cursor = getContentResolver().query(uri,projection,selection,args,order);
        if (cursor != null && cursor.moveToFirst())
            setCursor(cursor);
    }

    private void setCursor(Cursor cursor) {
        synchronized (this){
            mCursor = cursor;
            mNextCursor = cursor;
        }
    }

    private void setPlayPos(int position){
        synchronized (this){
            mPlayPos = position;
        }
    }

    private boolean isCurrentlyPlaying(){
        if (mPlayerState == PLAYER_STATE_ERROR)
            return  false;
        return mPlayer.isPlaying();
    }

    public static boolean isPlaying(){
        return mIsSupposedToBePlaying;
    }

    @Override
    public IBinder onBind(Intent intent) {
        mDelayedStopHandler.removeCallbacksAndMessages(null);
        mServiceBound = true;
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        mDelayedStopHandler.removeCallbacksAndMessages(null);
        mServiceBound = true;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mServiceBound = false;

        if (isPlaying() || mPausedByTransientLossOfFocus)
            return true;

        // if there is a playlist but playback is paused or we are transitioning between tracks
        if (mMediaPlayerHandler.hasMessages(TRACK_ENDED)){
            mDelayedStopHandler.sendMessageDelayed(mDelayedStopHandler.obtainMessage(TRACK_ENDED),IDLE_DELAY);
            return true;
        }
        synchronized (this){
            mStateHandler.sendEmptyMessage(PLAYER_STATE_END);
        }
        stopSelf(mServiceStartId);
        //mSession.release();
        return true;
    }

    //Instantiate Handlers
    private final MediaPlayerStateHandler mStateHandler = new MediaPlayerStateHandler();
    private final MediaPlayerHandler mMediaPlayerHandler = new MediaPlayerHandler();
    private final DelayedHandler mDelayedStopHandler = new DelayedHandler();
    private TaskHandler mTaskHandler;
    private BroadcastHandler mBroadcastHandler;





    //Create Handlers

    private class BroadcastHandler extends Handler {
        public BroadcastHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case NEXT:
                    goToNext(true);
                    break;
                case PREVIOUS:
                    previous();
                    break;
                case PLAY:
                    play();
                    break;
                case PAUSE:
                    pause();
                    break;
                case ADD_SONG:
                    setCurrentAndNextTrack();
                    play();
                    break;
            }
        }
    }

    private class DelayedHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            // Make sure nothing is playing at the moment
            if (isPlaying() || mPausedByTransientLossOfFocus || mServiceBound ||
                    mMediaPlayerHandler.hasMessages(TRACK_ENDED)){
                return;
            }

            synchronized (this){
                mStateHandler.sendEmptyMessage(PLAYER_STATE_END);
            }
            stopSelf(mServiceStartId);
        }
    }

    private class TaskHandler extends Handler {
        public TaskHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case PLAYLIST_CHANGED:
                    updateSongList();
                    break;
                case POSITION_CHANGED:
                    setPlayPos(msg.arg1);
                    break;
                case ADD_TO_HISTORY:
                    addToHistory();
                    break;
            }
        }
    }

    private class MediaPlayerStateHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case PLAYER_STATE_IDLE:
                    mPlayerState = PLAYER_STATE_IDLE;
                    break;
                case PLAYER_STATE_PREPARED:
                    mPlayerState = PLAYER_STATE_PREPARED;
                    break;
                case PLAYER_STATE_STARTED:
                    mPlayerState = PLAYER_STATE_STARTED;
                    break;
                case PLAYER_STATE_PAUSED:
                    mPlayerState = PLAYER_STATE_PAUSED;
                    break;
                case PLAYER_STATE_STOPPED:
                    mPlayerState = PLAYER_STATE_STOPPED;
                    break;
                case PLAYER_STATE_COMPLETE:
                    mPlayerState = PLAYER_STATE_COMPLETE;
                case PLAYER_STATE_END:
                    mPlayerState = PLAYER_STATE_END;
                    break;
                case PLAYER_STATE_ERROR:
                    mPlayerState = PLAYER_STATE_ERROR;
                    break;
            }
        }
    }


    private class MediaPlayerHandler extends Handler {
        private float mCurrentVolume = 1.0f;
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case FADE_IN:
                    if (isPlaying()){
                        mCurrentVolume += 0.05f;
                        if (mCurrentVolume < .1f){
                            mMediaPlayerHandler.sendEmptyMessageDelayed(FADE_IN, 10);
                        } else {
                            mCurrentVolume = 1f;
                        }
                        mPlayer.setVolume(mCurrentVolume);
                    }
                    break;
                case FADE_OUT:
                    if (isPlaying()){
                        mCurrentVolume -= 0.05f;
                        if (mCurrentVolume > 0.2f){
                            mMediaPlayerHandler.sendEmptyMessageDelayed(FADE_OUT, 10);
                        } else {
                            mCurrentVolume = 0.2f;
                        }
                        mPlayer.setVolume(mCurrentVolume);
                    }
                    break;
                case SERVER_DIED:
                    if (mIsSupposedToBePlaying) {
                        goToNext(true);
                    } else {
                        // Server died when we were idle
                        //Set data source for current and next track
                        setCurrentAndNextTrack(false);
                    }
                    break;
                case TRACK_WENT_TO_NEXT:
                    //TODO
                    notifyChange(METADATA_CHANGED);
                    updateNotification();
                    //Set Data source for next track
                    setNextTrack();
                    break;
                case TRACK_ENDED:
                    if(mRepeatMode == REPEAT_CURRENT){
                        synchronized (this){
                            seekTo(0);
                        }
                        play();
                    } else {
                        // Play the next track
                        goToNext(false);
                    }
                    break;
                case RELEASE_WAKELOCK:
                    mWakeLock.release();
                    break;
                case FOCUS_CHANGE:
                    switch (msg.arg1){
                        case AudioManager.AUDIOFOCUS_GAIN:
                            Log.v(LOGTAG, "AudioFocus gain received");
                            if (!isPlaying() && mPausedByTransientLossOfFocus){
                                mPausedByTransientLossOfFocus = false;
                                mCurrentVolume = 0f;
                                mPlayer.setVolume(mCurrentVolume);
                                play();
                            } else {
                                mMediaPlayerHandler.removeMessages(FADE_OUT);
                                mMediaPlayerHandler.sendEmptyMessage(FADE_IN);
                            }
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS:
                            Log.v(LOGTAG, "AudioFocus loss received");
                            if (!isPlaying()){
                                mPausedByTransientLossOfFocus = false;
                            }
                            pause();
                            break;
                        case  AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                            mMediaPlayerHandler.removeMessages(FADE_IN);
                            mMediaPlayerHandler.sendEmptyMessage(FADE_OUT);
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                            Log.v(LOGTAG, "AudioFocus loss transient received");
                            if (isPlaying()){
                                mPausedByTransientLossOfFocus = true;
                            }
                            pause();
                            break;
                        default:
                            Log.e(LOGTAG,"Received unknown code "+ msg.arg1 + " for AudioFocus change");
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void pause() {
        synchronized (this){
            if (mPlayerState == PLAYER_STATE_COMPLETE || mPlayerState == PLAYER_STATE_PAUSED || mPlayerState == PLAYER_STATE_STARTED){
                mMediaPlayerHandler.removeMessages(FADE_IN);
                if (isPlaying()){
                    mStateHandler.sendEmptyMessage(PLAYER_STATE_PAUSED);
                    mPlayer.pause();
                    goToIdleState();
                    mIsSupposedToBePlaying = false;
                    // TODO notify change?
                }
            }
            Log.d(LOGTAG, "player in state "+ mPlayerState);
        }

    }

    public long getDuration() {
        return mPlayer.getDuration();
    }

    /**
     * Returns the current playback position in milliseconds
     */
    public long getPosition() {
        if (mPlayer.isInitialized()) {
            return mPlayer.getPosition();
        }
        return ERROR;
    }

    public void setCurrentPlaylist(int currentPlaylist) {
        this.mCurrentPlaylist = currentPlaylist;
    }

    private void goToIdleState() {
        mDelayedStopHandler.removeCallbacksAndMessages(null);
        mDelayedStopHandler.sendMessageAtTime(mDelayedStopHandler.obtainMessage(),IDLE_DELAY);
        stopForeground(true);
    }

    private long seekTo(long position) {
        synchronized (this){
            if (mPlayerState != (PLAYER_STATE_IDLE|PLAYER_STATE_ERROR|PLAYER_STATE_STOPPED))
                return mPlayer.seekTo(position);
        }
        return ERROR;
    }


    private void updateNotification() {
        // TODO
    }

    private void notifyChange(String what) {

        Intent i = new Intent(what);
        i.putExtra("id", Long.valueOf(getAudioId()));
        i.putExtra("artist", getArtistName());
        i.putExtra("track", getTrackName());
        i.putExtra("playing", isPlaying());
        sendStickyBroadcast(i);

        // TODO update for remote control client

    }

    private Cursor getCursorForId(long lid) {
        String id = String.valueOf(lid);
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.TITLE
        };
        String order = MediaStore.Audio.Media.TITLE;

        Cursor c = getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection, "_id=" + id , null, order);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    /**
     * Gets the next previous Track and plays it
     * Depending on the shuffle mode, the song may be removed from
     * the history. It sets the previous song to the current song
     * and plays it
     */
    private void previous(){
        synchronized (this){
            if (mShuffleMode == SHUFFLE_AUTO){
                if (mSongHistory.isEmpty()){
                    return;
                }
                mCurrentSong.id = mSongHistory.remove(mSongHistory.size() -1);
                mHistoryPos--;
            } else {
                if (mHistoryPos < 1){
                    //Nothing to go back to
                    return;
                } else {
                    // Get the previous song from the song list
                    mCurrentSong.id = mSongHistory.get(--mHistoryPos);

                }
            }
        }
        stop(false);
        setCurrentAndNextTrack(true);
        play();
        notifyChange(METADATA_CHANGED);
    }

    /**
     * Default @setCurrentAndNextTrack() method
     * This method sets the data for the current player and the next player.
     * This method assumes that we need to update the current song and next
     * due to the use manually selecting a song
     */
    private void setCurrentAndNextTrack(){
        setCurrentAndNextTrack(false);
    }

    /**
     * Sets the data for the current player and the next player
     * This method assumes that we need to update the current and next song
     * but not due to user action such as manual selection but for reasons
     * such as the serer dying
     */
    private void setCurrentAndNextTrack(boolean fromHistory) {
        // TODO Note that this method does not take care of items that are deleted by user during use
        if (fromHistory) {
            // The user has selected previous
            // If we get here then there is a song so play from song history
            updateCurrentSongFromId();
            stop(false);
            synchronized (this){
                mPlayer.setDataSource(mCurrentSong.data);
            }
            if (mShuffleMode == SHUFFLE_NONE){
                // The next song might come from the history
                setNextTrackHistory();
            } else {
                // The next song comes from mCursor
                setNextTrack();
            }
        } else {
            synchronized (this) {
                while (true) {
                    if (open()) {
                        break;
                    }
                    // Opening the file has failed
                    if (mOpenFailedCount++ < 10 && mIsPlaylist) {
                        int pos = getNextPos(false);
                        if (pos < 0) {
                            goToIdleState();
                            if (mIsSupposedToBePlaying) {
                                mIsSupposedToBePlaying = false;
                            }
                            return;
                        }
                        mPlayPos = pos;
                        stop(false);
                        mPlayPos = pos;
                        synchronized (this){
                            mCursor.moveToPosition(mPlayPos);
                        }
                    } else {
                        mOpenFailedCount = 0;
                        Toast.makeText(this, R.string.playback_failed, Toast.LENGTH_SHORT).show();
                        Log.d(LOGTAG, "Failed to open file for playback");
                        goToIdleState();
                        if (mIsSupposedToBePlaying) {
                            mIsSupposedToBePlaying = false;
                        }
                        return;
                    }

                }
                setNextTrack();
            }
        }
    }

    private boolean open(){
        synchronized (this){
            updateCurrentSong();
            mPlayer.setDataSource(mCurrentSong.data);
            if (mPlayer.isInitialized()) {
                mOpenFailedCount = 0;
                return true;
            }
            stop(true);
            return false;
        }
    }


    /**
     * Sets the data source for the next track
     */
    private void setNextTrack() {
        synchronized (this){
            mNextCursor = mCursor;
            mNextPlayPos =  getNextPos(false);
            if (mNextPlayPos >= 0) {
                mCursor.moveToPosition(mNextPlayPos);
                mPlayer.setNextDataSource(mNextCursor.getString(
                        mNextCursor.getColumnIndex(MediaStore.Audio.Media.DATA)));
            } else {
                mPlayer.setNextDataSource(null);
            }
        }
    }

    /**
     * Sets the next track based on the history pos
     */
    private void setNextTrackHistory(){
        synchronized (this){
            // check the position in the history index
            if (mHistoryPos == NONE) {
                if (mShuffleMode == SHUFFLE_NONE){
                    // If we are not shuffling, the next song should be what the current position is
                    synchronized (this){
                        mNextCursor.moveToPosition(mPlayPos); //set next song to last known play position
                        String data = mNextCursor.getString(mNextCursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                        mPlayer.setNextDataSource(data);
                    }
                }
                else{
                    //Need to select a new song
                    setNextTrack();
                }
            } else {
                Cursor cursor = getCursorForId(mSongHistory.get(mHistoryPos));
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                mPlayer.setNextDataSource(data);
                mHistoryPos++;
            }
        }

    }

    private void updateCurrentSong() {
        synchronized (this){
            mCursor.moveToPosition(mPlayPos);
            if (mCursor!= null){
                synchronized (this){
                    mCurrentSong.id = mCursor.getInt(mCursor.getColumnIndex(MediaStore.Audio.Media._ID));
                    mCurrentSong.name = mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    mCurrentSong.artist = mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    mCurrentSong.albumId = mCursor.getLong(mCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                    mCurrentSong.data = mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                    Log.d(LOGTAG, "Current song is "+ mCurrentSong.data);
                }
            }
        }
        //Cursor cursor = getCursorForId(mCurrentSong.id);


    }

    private void updateCurrentSongFromId() {
        Cursor cursor = getCursorForId(mCurrentSong.id);
        if (cursor!= null){
            synchronized (this){
                mCurrentSong.name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                mCurrentSong.artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                mCurrentSong.albumId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                mCurrentSong.data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
            }
        }
    }



    private void stop(boolean removeStatusIcon){
        if (mPlayer != null && mPlayer.isInitialized()){
            synchronized (this) {
                mPlayer.stop();
            }
        }

        if (removeStatusIcon){
            goToIdleState();
        } else {
            stopForeground(true);
        }
        if (removeStatusIcon)
            mIsSupposedToBePlaying = false;
    }

    private void stop(){
        stop(true);
    }

    private final Vector<Integer> shuffleHistory = new Vector<>(1000); //starting capacity 1000
    private Shuffler mShuffler = new Shuffler();

    private static class  Shuffler {
        private int mPrevious;
        private Random mRandom = new Random();

        private int nextInt(int range){
            int result;
            do{
                result = mRandom.nextInt(range);
            }while (result == mPrevious && range > 1);
            mPrevious = result;
            return result;
        }
    }

    private void goToNext(boolean forceNext) {
        int pos = getNextPos(forceNext);
        if (pos < 0) {
            goToIdleState();
            if (mIsSupposedToBePlaying) {
                mIsSupposedToBePlaying = false;
                //notifyChange(PLAYSTATE_CHANGED);
            }
            return;
        }
        synchronized (this){
            mPlayPos = pos;
            stop(false);
            mPlayPos = pos;
        }
        setCurrentAndNextTrack();
        play();
    }


    /**
     * This method gets the next position that cursor should move to to retrieve the next song
     * Note: In this method the shuffle does not take into account the fact that the user could
     * shuffle midway i.e. if track 1-5 are played, shuffling does not shuffle between the remaining
     * unplayed tracks but rather shuffles them all, whereby they all start as unplayed.
     * @param forceNext Whether to get a valid position under all circumstances
     * @return          The Next position to played
     */
    private int getNextPos(boolean forceNext){
        if (mRepeatMode == REPEAT_CURRENT){
            if (mPlayPos < 0)
                return 0; // If you haven't played a song before, start from the beginning
            return mPlayPos;
        } else if (mShuffleMode == SHUFFLE_AUTO) {
            if (mPlayPos >= 0) { //If we are playing something
                shuffleHistory.add(mPlayPos);
            }
            // Keep ArrayList size to 1000 elements
            if (shuffleHistory.size() > 1000) {
                shuffleHistory.removeElementAt(0);
            }
            int numTracks = mCursor.getCount();

            int skipToPos;
            do {
                skipToPos = mShuffler.nextInt(numTracks);
            }while(shuffleHistory.contains(skipToPos)) ;
            return skipToPos;
        } else {
            if (mCursor.isLast()){
                if (mRepeatMode ==  REPEAT_ALL || forceNext){
                    return 0;
                } else if (mRepeatMode ==  REPEAT_NONE && !forceNext){
                    return FINISHED;
                }
            }  else{
                return mPlayPos + 1;
            }
        }
        return mPlayPos;
    }

    private void play(){
        synchronized (this){
            mAudioManager.requestAudioFocus(
                    mAudioFocusChangeListener, AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN);
            if (mPlayer.isInitialized()){
                long duration = mPlayer.getDuration();
                if (mRepeatMode != REPEAT_CURRENT && duration > 2000 &&
                        mPlayer.getPosition() >= duration - 2000) {
                    goToNext(true);
                }
                mPlayer.start();
                mTaskHandler.sendEmptyMessage(ADD_TO_HISTORY);
                mMediaPlayerHandler.removeMessages(FADE_OUT);
                mMediaPlayerHandler.sendEmptyMessage(FADE_IN);
                if (!mIsSupposedToBePlaying)
                    mIsSupposedToBePlaying = true;
            }
        }
    }

    // TODO Look into holding mutex for a shorter time
    private void addToHistory(){
        if (!mSongHistory.contains(mCurrentSong)){
            mSongHistory.add(mCurrentSong.id);
            mHistoryPos++;
        }

    }

    private String getTrackName(){
        synchronized (this){
            if (mCursor == null)
                return null;
            return mCurrentSong.name;
        }
    }
    private String getAlbumName(){
        synchronized (this){
            if (mCursor == null)
                return null;
            return mCurrentSong.albumName;
        }
    }
    private long getAlbumId(){
        synchronized (this){
            if (mCursor == null)
                return ERROR;
            return mCurrentSong.albumId;
        }
    }
    private String getArtistName(){
        synchronized (this){
            if (mCursor == null)
                return null;
            return mCurrentSong.artist;
        }
    }
    private long getArtistId(){return 0;}
    private String getPath(){
        synchronized (this){
            if (mCursor == null)
                return null;
            return mCurrentSong.data;
        }
    }
    private long getAudioId(){return 0;}
    private void setShuffleMode(int shuffleMode){
        this.mShuffleMode = shuffleMode;
    }
    private int getShuffleMode(){
        return this.mShuffleMode;
    }
    private int removeTracks(int first, int last){return 0;}
    private int removeTrack(long id){return 0;}
    private void setRepeatMode(int repeatMode){
        this.mRepeatMode = repeatMode;
    }
    private int getRepeatMode(){
        return mRepeatMode;
    }
    private int getMediaMountedCount(){return 0;}

    private int getAudioSessionId(){
        return mPlayer.getAudioSessionId();
    }

    private void setAudioSessionId(int sessionId){
        synchronized (this){
            mPlayer.setAudioSessionId(sessionId);
        }
    }


    private AudioManager.OnAudioFocusChangeListener mAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            mMediaPlayerHandler.obtainMessage(FOCUS_CHANGE,focusChange,0).sendToTarget();
        }
    };

    private class MultiPlayer{
        private SingleMediaPlayer mCurrentMediaPlayer = new SingleMediaPlayer();
        private SingleMediaPlayer mNextMediaPlayer;
        private Handler mHandler;
        private boolean mIsInitialized = false;

        public MultiPlayer(){
            mCurrentMediaPlayer.setWakeMode(AudioService.this,PowerManager.PARTIAL_WAKE_LOCK);
        }

        private void setDataSource(String path){
            mIsInitialized = setDataSourceImpl(mCurrentMediaPlayer, path);
            if (mIsInitialized){
                setNextDataSource(null, false);
            }
            else{
                //If at first you don't succeed, try again
                setNextDataSource(path, true);
            }
        }

        private void setNextDataSource(String path){
            setNextDataSource(path, false);
        }

        private void setNextDataSource(String path, boolean tryAgain){
            mCurrentMediaPlayer.setNextMediaPlayer(null);
            if (mNextMediaPlayer != null){
                mNextMediaPlayer.release();
                mNextMediaPlayer = null;
            }
            if (path == null)
                return;
            mNextMediaPlayer =  new SingleMediaPlayer();
            mNextMediaPlayer.setWakeMode(AudioService.this, PowerManager.PARTIAL_WAKE_LOCK);
            mNextMediaPlayer.setAudioSessionId(getAudioSessionId());
            if (setDataSourceImpl(mNextMediaPlayer, path)) {
                if(tryAgain){
                    switchOldToNewPlayer();
                    mIsInitialized = true;
                    setNextDataSource(null, false);
                } else {
                    mCurrentMediaPlayer.setNextMediaPlayer(mNextMediaPlayer);
                }
            } else {
                // failed to open next. File will be skipped
                mNextMediaPlayer.release();
                mNextMediaPlayer = null;
            }

        }

        private boolean setDataSourceImpl(MediaPlayer player, String path){
            try{
                player.reset();
                player.setOnPreparedListener(preparedListener);
                if (path.startsWith("content://")) {
                    player.setDataSource(AudioService.this, Uri.parse(path));
                } else {
                    player.setDataSource(path);
                }
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                player.prepare();
            } catch (IOException e) {
                return false;
            } catch (IllegalArgumentException e) {
                return false;
            }
            player.setOnCompletionListener(completionListener);
            player.setOnErrorListener(errorListener);
            return true;
        }

        private boolean isInitialized() {
            return mIsInitialized;
        }

        private void start() {
            mCurrentMediaPlayer.start();
            mStateHandler.sendEmptyMessage(PLAYER_STATE_STARTED);
        }

        private void stop() {
            mStateHandler.sendEmptyMessage(PLAYER_STATE_IDLE);
            mCurrentMediaPlayer.reset();
            mIsInitialized = false;
        }

        private void release() {
            stop();
            mStateHandler.sendEmptyMessage(PLAYER_STATE_END);
            mCurrentMediaPlayer.release();
        }

        private void pause() {
            mStateHandler.sendEmptyMessage(PLAYER_STATE_PAUSED);
            mCurrentMediaPlayer.pause();
        }

        private void setHandler(Handler handler) {
            mHandler = handler;
        }

        private boolean isPlaying(){
            return mCurrentMediaPlayer.isPlaying();
        }

        private long getDuration() {
            return mCurrentMediaPlayer.getDuration();
        }

        private long getPosition() {
            return mCurrentMediaPlayer.getCurrentPosition();
        }

        private long seekTo(long whereto) {
            mCurrentMediaPlayer.seekTo((int) whereto);
            return whereto;
        }

        private void setVolume(float vol) {
            mCurrentMediaPlayer.setVolume(vol, vol);
        }

        private int getAudioSessionId(){
            return mCurrentMediaPlayer.getAudioSessionId();
        }

        private void setAudioSessionId(int sessionId){ mCurrentMediaPlayer.setAudioSessionId(sessionId);}


        MediaPlayer.OnPreparedListener preparedListener =  new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                if (mp == mCurrentMediaPlayer)
                    mStateHandler.sendEmptyMessage(PLAYER_STATE_PREPARED);
                //TODO audio effect control session?
            }
        };

        MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mStateHandler.sendEmptyMessage(PLAYER_STATE_COMPLETE);
                if (mp == mCurrentMediaPlayer && mNextMediaPlayer != null){
                    switchOldToNewPlayer();
                    mStateHandler.sendEmptyMessage(PLAYER_STATE_PREPARED);
                    mHandler.sendEmptyMessage(TRACK_WENT_TO_NEXT);
                }
                else{
                    mWakeLock.acquire(30000);
                    mHandler.sendEmptyMessage(TRACK_ENDED);
                    mHandler.sendEmptyMessage(RELEASE_WAKELOCK);
                }

            }
        };

        private void switchOldToNewPlayer() {
            mCurrentMediaPlayer.release();
            mCurrentMediaPlayer = mNextMediaPlayer;
            mNextMediaPlayer = null;
        }

        MediaPlayer.OnErrorListener errorListener = new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                mStateHandler.sendEmptyMessage(PLAYER_STATE_ERROR);
                switch (what){
                    case  MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                        mIsInitialized = false;
                        mCurrentMediaPlayer.release();
                        mCurrentMediaPlayer =  new SingleMediaPlayer();
                        mStateHandler.sendEmptyMessage(PLAYER_STATE_IDLE);
                        mCurrentMediaPlayer.setWakeMode(AudioService.this, PowerManager.PARTIAL_WAKE_LOCK);
                        mHandler.sendEmptyMessage(SERVER_DIED);
                        return true;
                    default:
                        Log.d(LOGTAG, "Multipler Error: " +  what + "," + extra);
                        break;
                }
                return false;
            }
        };
    }


    private static class SingleMediaPlayer extends MediaPlayer implements MediaPlayer.OnCompletionListener {

        private boolean mSingleMode = true;
        private MediaPlayer mNextPlayer;
        private OnCompletionListener mCompletion;

        public SingleMediaPlayer(){
            try{
                MediaPlayer.class.getMethod("setNextMediaPlayer", MediaPlayer.class);
                mSingleMode = false;
            } catch (NoSuchMethodException e) {
                mSingleMode = true;
                super.setOnCompletionListener(this);
            }
        }

        public void setNextMediaPlayer(MediaPlayer nextPlayer){
            if (mSingleMode){
                mNextPlayer = nextPlayer;
            } else {
                super.setNextMediaPlayer(nextPlayer);
            }
        }

        @Override
        public void onCompletion(MediaPlayer mp) {
            if (mNextPlayer != null){
                // as it turns out, starting a new MediaPlayer on the completion
                // of a previous player ends up slightly overlapping the two
                // playbacks, so slightly delaying the start of the next player
                // gives a better user experience
                SystemClock.sleep(50);
                mNextPlayer.start();
            }
            mCompletion.onCompletion(this);
        }
    }

    static class ServiceStub extends IMediaPlaybackService.Stub {
        WeakReference<AudioService> mService;

        ServiceStub(AudioService service) {
            mService = new WeakReference<AudioService>(service);
        }

        public boolean isPlaying() {
            return mService.get().isPlaying();
        }

        @Override
        public void openFile(String path) throws RemoteException {

        }

        @Override
        public void open(long[] list, int position) throws RemoteException {

        }

        @Override
        public int getQueuePosition() throws RemoteException {
            return 0;
        }

        @Override
        public void stop() throws RemoteException {
            mService.get().stop();
        }

        @Override
        public void pause() throws RemoteException {
            mService.get().pause();
        }

        @Override
        public void play() throws RemoteException {
            mService.get().play();
        }

        @Override
        public void prev() throws RemoteException {
            mService.get().mBroadcastHandler.sendEmptyMessage(PREVIOUS);
        }

        @Override
        public void next() throws RemoteException {
            mService.get().mBroadcastHandler.sendEmptyMessage(NEXT);

        }
        @Override
        public long getDuration() {
            return mService.get().getDuration();
        }
        @Override
        public long seekTo(long position) {
            return mService.get().seekTo(position);
        }
        @Override
        public String getTrackName(){return mService.get().getTrackName();}
        @Override
        public String getAlbumName(){return mService.get().getAlbumName();}
        @Override
        public long getAlbumId(){return mService.get().getAlbumId();}
        @Override
        public String getArtistName(){return mService.get().getArtistName();}
        @Override
        public long getArtistId(){return mService.get().getArtistId();}
        @Override
        public String getPath(){return mService.get().getPath();}

        @Override
        public void enqueue(long[] list, int action) throws RemoteException {

        }

        @Override
        public long[] getQueue() throws RemoteException {
            return new long[0];
        }

        @Override
        public void moveQueueItem(int from, int to) throws RemoteException {

        }

        @Override
        public void setQueuePosition(int index) throws RemoteException {

        }
        @Override
        public long getAudioId(){
            return mService.get().getAudioId();
        }
        @Override
        public void setShuffleMode(int shuffleMode){
            mService.get().setShuffleMode(shuffleMode);
        }
        @Override
        public int getShuffleMode(){
            return mService.get().getShuffleMode();
        }
        //TODO Implement track batch removal
        @Override
        public int removeTracks(int first, int last){return 0;}
        @Override
        //TODO Implement track removal
        public int removeTrack(long id){return 0;}
        @Override
        public void setRepeatMode(int repeatMode){
            mService.get().setRepeatMode(repeatMode);
        }
        @Override
        public int getRepeatMode(){
            return mService.get().getRepeatMode();
        }
        @Override
        public int getMediaMountedCount(){return 0;}
        @Override
        public int getAudioSessionId(){
            return mService.get().getAudioSessionId();
        }

    }

    private final IBinder mBinder = new ServiceStub(this);

}

// TODO - Handle dealing with audio output hardware