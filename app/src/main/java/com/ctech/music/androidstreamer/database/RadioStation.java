package com.ctech.music.androidstreamer.database;

/**
 * Created by EGATCHA on 3/24/2015.
 */


public class RadioStation {

    int id;
    String title;
    String subtitle;
    String genre;
    String bitrate;
    String uri;

    public RadioStation(int id, String title, String uri){
        this.id =id;
        this.title = title;
        this.uri = uri;
    }

    public RadioStation(int id, String title, String subtitle, String uri, String genre, String bitrate){
        this.title =title;
        this.id = id;
        this.subtitle = subtitle;
        this.uri = uri;
        this.genre = genre;
        this.bitrate = bitrate;
    }

    public void setBitrate(String bitrate){
        this.bitrate = bitrate;
    }

    public void setSubtitle(String subtitle){
        this.subtitle = subtitle;
    }

    public void setGenre(String genre){
        this.genre = genre;
    }

    public void setId(int id){
        this.id =id;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setUri(String uri){
        this.uri = uri;
    }

    public String setBitrate() {
        return this.bitrate;
    }

    public  int getId(){
        return this.id;
    }

    public String getTitle(){
        return this.title;
    }

    public String getSubtitle(){
        return this.subtitle;
    }

    public String getUri(){
        return this.uri;
    }

    public String getBitrate(){
        return this.bitrate;
    }

    public String getGenre(){
        return this.genre;
    }
}