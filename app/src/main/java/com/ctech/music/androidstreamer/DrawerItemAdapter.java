package com.ctech.music.androidstreamer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by Charles on 17/03/2015.
 */
public class DrawerItemAdapter extends RecyclerView.Adapter<DrawerItemAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private Context context;
    List<DrawerItemData> data = Collections.emptyList();

    public DrawerItemAdapter(Context context, List<DrawerItemData> data){
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.drawer_item, parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(DrawerItemAdapter.MyViewHolder holder, int position) {
        DrawerItemData current = data.get(position);
        holder.title.setText(current.getTitle());
        holder.icon.setImageResource(current.getIconPath());

    }

    @Override
    public int getItemCount() {

        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        ImageView icon;

        public MyViewHolder(View itemView) {

            super(itemView);
            title = (TextView) itemView.findViewById(R.id.drawer_item_text);
            icon = (ImageView) itemView.findViewById(R.id.drawer_item_image);
        }

    }
}
