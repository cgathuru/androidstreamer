package com.ctech.music.androidstreamer;

import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ctech.music.androidstreamer.android.widget.MySeekBar;

/**
 * Created by Charles on 24/03/2015.
 */
public interface MusicPlayer {

    public TextView getTimeElapsed();

    public ImageButton getAlbumArt();

    public TextView getDuration();

    public TextView getBitrate();

    public MySeekBar getSeekBar();

    public TextView getSongTitle();

    public TextView getArtist();

    public ImageButton getPausePlayButton();

    public ImageButton getPreviousButton();

    public ImageButton getNextButton();

}
