package com.ctech.music.androidstreamer;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Charles on 20/03/2015.
 */
public class Song implements Parcelable {

    @Override
    public String toString() {
        return "Song [id=" + id + ", subtitle=" + artist + ", name=" + name
                + ", data=" + data + "]";
    }

    public int id;
    public String artist;
    public String name;
    public String data;
    public String albumName;
    public String albumKey;
    public long albumId;
    public String albumArtUri;
    public String genre;

    public Song(int id, String name, String artist, String data,
                String albumName, String albumKey, Long albumId, String genre) {
        this.id = id;
        this.name = name;
        this.artist = artist;
        this.data = data;
        this.albumName = albumName;
        this.albumKey = albumKey;
        this.albumId = albumId;
        this.genre = genre;

    }

    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {

        public Song createFromParcel(Parcel source) {
            // TODO Auto-generated method stub
            return new Song(source);
        }

        public Song[] newArray(int size) {
            // TODO Auto-generated method stub
            return new Song[size];
        }

    };

    public Song(Parcel source) {
        // TODO Auto-generated constructor stub
        readFromParcel(source);
    }

    public Song() {
    }

    public int describeContents() {
        // TODO Auto-generated method stub
        return 7;
    }

    public void readFromParcel(Parcel in) {
        id = in.readInt();
        artist = in.readString();
        name = in.readString();
        data = in.readString();
        albumName = in.readString();
        albumKey = in.readString();
        albumId = in.readLong();
        albumArtUri = in.readString();
        genre = in.readString();
    }

    public void writeToParcel(Parcel out, int arg1) {
        out.writeInt(id);
        out.writeString(artist);
        out.writeString(name);
        out.writeString(data);
        out.writeString(albumName);
        out.writeString(albumKey);
        out.writeLong(albumId);
        out.writeString(albumArtUri);
        out.writeString(genre);
    }
}
