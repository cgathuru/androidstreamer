package com.ctech.music.androidstreamer;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ctech.music.androidstreamer.android.widget.MySeekBar;
import com.ctech.music.androidstreamer.Utilities.ServiceToken;
import com.squareup.picasso.Picasso;

import wseemann.media.FFmpegMediaMetadataRetriever;

/**
 * Created by Charles on 21/03/2015.
 */
public abstract class BaseActivity extends ActionBarActivity implements Communicator,ServiceConnection {

    public static final int SEEK_BAR_MAX = 1000;
    private static final String LOGTAG = "BaseActivity";
    public static final String UPDATE_SEEKBAR = "com.ctech.music.androidstreamer.UPDATE_SEEKBAR";
    public static final String UPDATE_VIEW = "com.ctech.music.androidstreamer.UPDATE_VIEW";
    public static final int DEFAULT_SEEK_BAR_WIDTH = 320;
    public static final int OPTIMAL_REFRESH_RATE = 20;

    protected ImageButton albumArt;
    protected TextView timeElapsed;
    protected TextView duration;
    protected TextView bitrate;
    protected MySeekBar seekBar;
    protected TextView songTitle;
    protected TextView artist;
    protected ImageButton pausePlayButton;
    protected ImageButton previousButton;
    protected ImageButton nextButton;

    protected AudioService audioService;
    protected LocalBroadcastManager bManager;
    protected BitmapManager bitmapManager;
    protected android.support.v4.app.FragmentManager manager;
    protected MusicPlayerFragment musicPlayer;
    //private ServiceToken mToken;



    protected long mDuration;
    protected long mLastSeekEventTime;
    protected long mPosOverride = -1;

    Handler progressBarHandler = new Handler();

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(UPDATE_SEEKBAR)) {
                final long currentPosition = intent
                        .getLongExtra(Constants.CURRENT_POSITION, 0);
                final long duration = intent.getLongExtra(Constants.DURATION, 0);
                mDuration = duration;
                progressBarHandler .post(new Runnable() {

                    public void run() {
                        seekBar.setProgress((int) currentPosition);
                    }
                });
                timeElapsed.setText(Utilities.makeTimeString(currentPosition));
            } else if (action.equals(UPDATE_VIEW)) {
                long dur = intent.getLongExtra(Constants.DURATION, 0);
                Song nowPlaying = new Song();
                nowPlaying.name = intent.getStringExtra(Constants.NOW_PLAYING_TITLE);
                nowPlaying.artist = intent.getStringExtra(Constants.NOW_PLAYING_ARTIST);
                nowPlaying.data = intent.getStringExtra(Constants.DATA);
                new SongChanger().execute(nowPlaying);
                Log.i(LOGTAG, "Update View called");
                setDuration(dur);
                seekBar.setMax((int)dur);
            } /*else if (action.equals(OldService.REPEAT_ALL)) {
                long dur = intent.getLongExtra(Constants.DURATION, 0);
                setDuration(dur);
                resetPausePlayButton();
            }*/

        }
    };


    private class SongChanger extends AsyncTask<Song, Void, Object[]> {

        @Override
        protected Object[] doInBackground(Song... songs) {

            String data = songs[0].data;
            FFmpegMediaMetadataRetriever ffmr = new FFmpegMediaMetadataRetriever();
            ffmr.setDataSource(data);
            String title = ffmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_TITLE);
            if (title != null )
                songs[0].name = title;
            String artist = ffmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ARTIST);
            if (artist != null)
                songs[0].artist = artist;

            Object[] details = new Object[3];
            details[0] = songs[0].name;
            details[1] = songs[0].artist;
            details[2] = songs[0].data;

            return details;
        }

        @Override
        protected void onPostExecute(Object[] result) {
            songTitle.setText((CharSequence) result[0]);
            artist.setText((CharSequence) result[1]);
            /*if (result[2] != null) {
                albumArt.setImageBitmap((Bitmap) result[2]);
            } else {
                albumArt.setImageResource(R.drawable.android10);
            }*/
            Picasso.with(getApplicationContext()).load((String)result[2]).fit().noFade().into(albumArt);
            // TODO Send broadcast so that the song can be added to history
        }
    }

    @Override
    public void communicate(final View view) {
        Log.i(LOGTAG,"Established communicator Link");
        musicPlayer = (MusicPlayerFragment) manager.findFragmentById(R.id.music_player_fragment_container);
        albumArt = musicPlayer.getAlbumArt();
        if (albumArt == null){
            throw new ArrayIndexOutOfBoundsException("Not working");
        }
        albumArt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onArtClick(view);
            }
        });
        timeElapsed = musicPlayer.getTimeElapsed();
        duration = musicPlayer.getDuration();
        bitrate = musicPlayer.getBitrate();
        seekBar = musicPlayer.getSeekBar();
        songTitle = musicPlayer.getSongTitle();
        artist = musicPlayer.getArtist();
        pausePlayButton = musicPlayer.getPausePlayButton();
        pausePlayButton.setImageResource(R.drawable.ic_action_name);
        pausePlayButton.setOnClickListener(new View.OnClickListener() {
            //TODO Update image resources with correct resources
            @Override
            public void onClick(View v) {
                try {
                    if (Utilities.sService.isPlaying()) {
                        bManager.sendBroadcast(new Intent(AudioService.ACTION_PAUSE));
                        Picasso.with(getApplicationContext()).load(R.drawable.ic_action_name).into(pausePlayButton);
                        //pausePlayButton.setImageResource(R.drawable.ic_action_name);
                    } else {
                        bManager.sendBroadcast(new Intent(AudioService.ACTION_PLAY));
                        Picasso.with(getApplicationContext()).load(R.drawable.ic_action_name).into(pausePlayButton);
                        //pausePlayButton.setImageResource(R.drawable.ic_action_name);
                }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

            }
        });

        previousButton = musicPlayer.getPreviousButton();
        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bManager.sendBroadcast(new Intent(AudioService.ACTION_PREVIOUS));
            }
        });
        nextButton = musicPlayer.getNextButton();
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bManager.sendBroadcast(new Intent(AudioService.ACTION_NEXT));
            }
        });

        if (seekBar instanceof SeekBar){
            SeekBar seeker = seekBar;
            seeker.setOnSeekBarChangeListener(seekBarListener);
        }
        seekBar.setVisibility(SeekBar.VISIBLE);
        seekBar.setMax(SEEK_BAR_MAX);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        //mToken = Utilities.bindToService(this, this);
        bitmapManager = new BitmapManager(this);
        bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter iFilter = new IntentFilter();
        iFilter.addAction(UPDATE_SEEKBAR);
        iFilter.addAction(UPDATE_VIEW);
        //iFilter.addAction(AudioService.REFRESH_ALL);
        // register the receiver
        bManager.registerReceiver(receiver,iFilter);
        manager =  getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        musicPlayer = new MusicPlayerFragment();
        transaction.replace(R.id.music_player_fragment_container, musicPlayer);
        transaction.commit();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

    }

    @Override
    protected void onPause() {
        //Intent boundState = new Intent(AudioService.BOUND_STATE_CHANGED);
        //boundState.putExtra(Constants.IS_BOUND,false);
        //bManager.sendBroadcast(boundState);
        super.onPause();

    }

    @Override
    public void onDestroy() {
        //Utilities.unbindFromService(mToken);
        if (AudioService.isPlaying()){
            //stopService(new Intent(this, AudioService.class));
            bManager.unregisterReceiver(receiver);
        }
        super.onDestroy();
    }

    protected abstract void onArtClick(View v);

    SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (audioService == null){
                return;
            }
            long currentTime = SystemClock.elapsedRealtime();
            if (fromUser){
                if ((currentTime - mLastSeekEventTime) > 250 ){
                    mLastSeekEventTime = currentTime;
                    try {
                        Utilities.sService.seekTo(progress);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    seekBar.setProgress(progress);
                }
            }
            else{
                mPosOverride = -1;
                //refresh();
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            mLastSeekEventTime = 0;

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            mPosOverride = -1;
        }
    };

   /* protected long refresh() {
        final long DEFAULT_REFRESH = 500;
        if (audioService == null){
            return DEFAULT_REFRESH;
        }
        long position = mPosOverride < 0 ? audioService.getPosition() : mPosOverride;
        if ((position >= 0) && (mDuration > 0)){
            int progress = (int) (SEEK_BAR_MAX * position / mDuration);
            //seekBar.setProgress(progress);
            
            if ( !audioService.isPlaying()) {
                return DEFAULT_REFRESH;
            }
        } else {
            //seekBar.setProgress(SEEK_BAR_MAX);
        }
        long timeRemaining = SEEK_BAR_MAX - (position % SEEK_BAR_MAX);

        // Estimate frequency to move seekBar to see smooth movement
        int width = seekBar.getWidth();
        if (width == 0)
            width = DEFAULT_SEEK_BAR_WIDTH;
        long refreshTime = mDuration/ width;

        if (refreshTime > timeRemaining)
            return timeRemaining;
        if (refreshTime < OPTIMAL_REFRESH_RATE)
            return OPTIMAL_REFRESH_RATE;

        return refreshTime;
    }*/

    /*public AudioService getService() {
        return audioService;
    }*/

    protected void resetPausePlayButton() {
        //TODO Update drawables to use actual icons
        if (audioService.isPlaying()){
            pausePlayButton.setImageResource(R.drawable.ic_action_name);
        }
        else{
            pausePlayButton.setImageResource(R.drawable.ic_action_name);
        }
    }

    protected void setDuration(long dur) {
        mDuration = dur;
        duration.setText(Utilities.makeTimeString(dur));
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {

    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        finish();
    }
}
