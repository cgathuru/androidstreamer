package com.ctech.music.androidstreamer;/**
 * Created by Charles Gathuru on 3/24/2015.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * The SongHolderClass holds a Song
 */
public class SongHolder extends RecyclerView.ViewHolder {

    ImageView albumArt;
    TextView title;
    TextView subtitle;
    ImageView nowPlaying;

    public SongHolder(View itemView) {
        super(itemView);

        albumArt = (ImageView) itemView.findViewById(R.id.item_album_art);
        title = (TextView) itemView.findViewById(R.id.item_title);
        subtitle = (TextView) itemView.findViewById(R.id.item_subtitle);
        nowPlaying = (ImageView) itemView.findViewById(R.id.item_playing_icon);
    }
}
