package com.ctech.music.androidstreamer.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ctech.music.androidstreamer.database.RadioStationContract.RadioStationEntry;

/**
 * Created by EGATCHA on 3/24/2015.
 */


public class RadioStationDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "RadioStations.db";

    public RadioStationDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
       final String TEXT_TYPE = " TEXT";
       final String COMMA_SEP = ",";
       final String NOT_NULL = " NOT NULL";
       final String INTEGER = " INTEGER";
       final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + RadioStationEntry.TABLE_NAME + " (" +
                        RadioStationEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        RadioStationEntry.COLUMN_NAME_RADIO_STATION_ID + INTEGER + NOT_NULL + COMMA_SEP +
                        RadioStationEntry.COLUMN_NAME_TITLE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                        RadioStationEntry.COLUMN_NAME_SUBTITLE + TEXT_TYPE + COMMA_SEP +
                        RadioStationEntry.COLUMN_NAME_GENRE + TEXT_TYPE + COMMA_SEP +
                        RadioStationEntry.COLUMN_NAME_BITRATE + TEXT_TYPE + COMMA_SEP +
                        RadioStationEntry.COLUMN_NAME_BITRATE + TEXT_TYPE + COMMA_SEP +
                        RadioStationEntry.COLUMN_NAME_URI + TEXT_TYPE + NOT_NULL +
                        " )";

        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + RadioStationEntry.TABLE_NAME);
        onCreate(db);

    }
}
