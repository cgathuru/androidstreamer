package com.ctech.music.androidstreamer;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ctech.music.androidstreamer.android.widget.MySeekBar;

/**
 * Created by Charles on 22/03/2015.
 */
public class MusicPlayerFragment extends Fragment implements MusicPlayer{

    private ImageButton albumArt;
    private TextView timeElapsed;
    private TextView duration;
    private TextView bitrate;
    private MySeekBar seekBar;
    private TextView songTitle;
    private TextView artist;
    private ImageButton pausePlayButton;
    private ImageButton previousButton;
    private ImageButton nextButton;
    private View view;

    public static final String LOGTAG = "MusicPlayerFragment";
    public static final String ADD_SONG = "com.ctech.music.androidstreamer.MusicPlayerFragment.ADD_SONG";

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main_activity_music_player, container,false);
        albumArt = (ImageButton) view.findViewById(R.id.now_playing_album_art);
        timeElapsed = (TextView) view.findViewById(R.id.now_playing_time_elapsed);
        duration = (TextView) view.findViewById(R.id.now_playing_duration);
        bitrate = (TextView) view.findViewById(R.id.now_playing_bitrate);
        seekBar = (MySeekBar) view.findViewById(R.id.now_playing_seek_bar);
        songTitle = (TextView) view.findViewById(R.id.now_playing_title);
        artist = (TextView) view.findViewById(R.id.now_playing_artist);
        pausePlayButton = (ImageButton) view.findViewById(R.id.now_playing_play_pause);
        previousButton = (ImageButton) view.findViewById(R.id.now_playing_previous);
        nextButton = (ImageButton) view.findViewById(R.id.now_playing_next);
        return view;
    }

    private void Check(){

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            Communicator comm = (Communicator) getActivity();
            comm.communicate(view);
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    @Override
    public TextView getTimeElapsed() {
        return this.timeElapsed;
    }

    @Override
    public ImageButton getAlbumArt() {
        return this.albumArt;
    }

    @Override
    public TextView getDuration() {
        return this.duration;
    }

    @Override
    public TextView getBitrate() {
        return this.bitrate;
    }

    @Override
    public MySeekBar getSeekBar() {
        return this.seekBar;
    }

    @Override
    public TextView getSongTitle() { return this.songTitle; }

    @Override
    public TextView getArtist() {
        return this.artist;
    }

    @Override
    public ImageButton getPausePlayButton() {
        return this.pausePlayButton;
    }

    @Override
    public ImageButton getPreviousButton() {
        return this.previousButton;
    }

    @Override
    public ImageButton getNextButton() {
        return this.nextButton;
    }
}
