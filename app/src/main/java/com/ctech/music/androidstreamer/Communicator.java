package com.ctech.music.androidstreamer;

import android.view.View;

/**
 * Created by Charles on 24/03/2015.
 */
public interface Communicator {

    public void communicate(View view);
}
