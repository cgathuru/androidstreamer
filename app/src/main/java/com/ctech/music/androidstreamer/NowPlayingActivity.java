package com.ctech.music.androidstreamer;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Charles on 21/03/2015.
 */
public class NowPlayingActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_now_playing);

    }

    @Override
    protected void onArtClick(View v) {
        //TODO Open the information fragment
    }
}
