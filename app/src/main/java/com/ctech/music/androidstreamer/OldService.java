package com.ctech.music.androidstreamer;

public class OldService{

}


/*
package com.ctech.music.androidstreamer;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

import static com.ctech.music.androidstreamer.Constants.ADD_TO_PLAYING_NOW;
import static com.ctech.music.androidstreamer.Constants.ADD_TO_SONG_LIST;
import static com.ctech.music.androidstreamer.Constants.CURRENT_PLAYLIST;
import static com.ctech.music.androidstreamer.Constants.CURRENT_POSITION;
import static com.ctech.music.androidstreamer.Constants.DURATION;
import static com.ctech.music.androidstreamer.Constants.PLAY_NOW;
import static com.ctech.music.androidstreamer.Constants.POSITION;
import static com.ctech.music.androidstreamer.Constants.REPEAT_MODE;
import static com.ctech.music.androidstreamer.Constants.SET_IN_PLAYER;
import static com.ctech.music.androidstreamer.Constants.SHUFFLE_MODE;
import static com.ctech.music.androidstreamer.Constants.SONG;

*/
/**
 * Created by Charles on 21/03/2015.
 *//*

public class AudioService extends Service implements AudioManager.OnAudioFocusChangeListener
{
    public static final int NOTIFICATION_ID = 1234;
    Activity activity;
    public static final String ACTION = "action";
    public static final String ACTION_PLAY = "com.ctech.music.androidstreamer.PLAY";
    public static final String ACTION_PAUSE = "com.ctech.music.androidstreamer.PAUSE";
    public static final String ACTION_STOP = "com.ctech.music.androidstreamer.STOP";
    public static final String ACTION_NEXT = "com.ctech.music.androidstreamer.NEXT";
    public static final String ACTION_PREVIOUS = "com.ctech.music.androidstreamer.PREVIOUS";
    public static final String REFRESH_ALL = "com.ctech.music.androidstreamer.REFRESH_ALL";
    public static final String REMOVE_SONG = "com.ctech.music.androidstreamer.REMOVE_SONG";
    public static final String PLAY_STATE_CHANGED = "com.ctech.music.androidstreamer.PLAY_STATE_CHANGED";
    public static final String BOUND_STATE_CHANGED = "com.ctech.music.androidstreamer.BOUND_STATE_CHANGED";

    public static final String SERVICECMD = "com.ctech.music.androidstreamer.musicservicecommand";
    public static final String CMDNAME = "command";
    public static final String CMDTOGGLEPAUSE = "togglepause";
    public static final String CMDSTOP = "stop";
    public static final String CMDPAUSE = "pause";
    public static final String CMDPREVIOUS = "previous";
    public static final String CMDNEXT = "next";

    private static final String DATABASE_FETCHER = "database_fetcher";

    private boolean bound;

    public static final int BOUND_STATE_UNBOUND = 0;
    public static final int BOUND_STATE_BOUND = 1;

    private static final int FINISHED = -1;

    public static final int SHUFFLE_NONE = 0;
    public static final int SHUFFLE_NORMAL = 1;
    private int shuffleMode = 0 ;

    public static final int REPEAT_NONE = 0;
    public static final int REPEAT_CURRENT = 1;
    public static final int REPEAT_ALL = 2;
    private int repeatMode = 0;

    public static final int REFRESH_ACTIVITY = 0;
    public static final int REFRESH_PLAYER_FRAGMENT = 1;
    public static final int REFRESH_NOW_PLAYING_FRAGMENT = 2;

    public static final int NONE = -1;

    private static boolean mIsInitialized;
    private static boolean isPlayingSong = false;
    private static boolean addToHistroy = true;

    private static int mHistoryPos = -1;

    public static MediaPlayer mMediaPlayer;
    private NotificationManagerCompat notificationManager;

    private ArrayList<Song> songHistory = new ArrayList<Song>();
    //private ArrayList<Song> songHistory= new ArrayList<Song>();
    private int mPlayPos;
    private Cursor mCursor;

    private Song mCurrentSong;

    private int mPlaylistSelection = PLAYLIST_SONG;
    public static final int PLAYLIST_SONG = 0;
    public static final int PLAYLIST_ARTIST = 1;
    public static final int PLAYLIST_ARTIST_SPECIFIC = 2;
    public static final int PLAYLIST_ALBUM = 3;
    public static final int PLAYLIST_ALBUM_SPECIFIC = 4;
    public static final int PLAYLIST_GENRE = 5;
    public static final int PLAYLIST_GENRE_SPECIFIC = 6;

    //Handler messages
    private static final int PLAYLIST_CHANGED = 0;
    private static final int SERVER_DIED = 1;
    private static final int FADE_DOWN = 2;
    private static final int FADE_UP = 3;
    private static final int POSITION_CHANGED = 4;

    private final IBinder myBinder = new LocalBinder();

    private LocalBroadcastManager bManager;
    private AudioManager aManager;
    private int requestFocusResult;

    private static final String LOGTAG = "AudioService";
    public static final String PREFTAG = "AndroidStreamer";

    private Handler mHandler;

    // TODO Handle play state changes such as repeat mode and shuffle mode

    // TODO Handle queries to the database to get a list of ids to play

    private class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case PLAYLIST_CHANGED:
                    updateSongList();
                    break;
                case POSITION_CHANGED:
                    setPlayPos(msg.arg1);
                    break;
            }
        }
    }


    private Handler mMediaplayerHandler =  new Handler(){

    };
    private void updateSongList(){
        synchronized (this){
            //TODO reqery for genres and artists
            Song currentSong = mCurrentSong;

            Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            String order = MediaStore.Audio.Media.TITLE + " ASC";
            String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
            String[] selectionArgs = {""};
            String[] projection = {
                    MediaStore.Audio.Media._ID,
                    MediaStore.Audio.Media.ARTIST,
                    MediaStore.Audio.Media.TITLE,
                    MediaStore.Audio.Media.DATA,
                    MediaStore.Audio.Media.DURATION,
                    MediaStore.Audio.Media.ALBUM_ID,
                    MediaStore.Audio.Media.ALBUM_KEY,
                    MediaStore.Audio.Media.ALBUM
            };
            switch (mPlaylistSelection){
                case PLAYLIST_SONG:
                    order = MediaStore.Audio.Media.TITLE + " ASC";
                    break;
                case PLAYLIST_ALBUM:
                    order = MediaStore.Audio.Media.ALBUM + " ASC";
                    break;
                case PLAYLIST_ALBUM_SPECIFIC:
                    //We already know what the album is
                    order = MediaStore.Audio.Media.TITLE + " ASC";
                    break;
                case PLAYLIST_GENRE:
                    musicUri = MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI;
                    order =  MediaStore.Audio.Genres.NAME;
                    String [] newProjection = {
                            MediaStore.Audio.Genres._ID,
                            MediaStore.Audio.Genres.NAME,
                    };
                    projection = newProjection;
                    break;

            }

            Cursor songCursor = getContentResolver().query(
                    musicUri,projection,selection,null,
                    order);

            // If the cursor is valid, the use the new cursor
            if (songCursor !=null && songCursor.moveToFirst()){
                mCursor = songCursor;
                Log.d(LOGTAG, "Acquired Service cursor");
            }

            //TODO Complete update song list
        }
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // Determine what to do on each other the actions
            if (action.equals(AudioService.ACTION_PLAY)){
                if (isPlayingSong)
                    play();
            } else if(action.equals(AudioService.ACTION_PAUSE)){
                if (isPlaying()){
                    pause();
                }
            } else if (action.equals(AudioService.ACTION_NEXT)) {
                shouldUpdate =false;
                Log.i(LOGTAG, "Next Pressed");
                //TODO Handle next song
                Intent nextIntent = new Intent(MusicPlayerFragment.ADD_SONG);
                Song nextSong = new Song();
                if (mHistoryPos == -1){ //If the user just hits next after opening app , so no history
                    mCursor.moveToFirst();
                    Log.d(LOGTAG, "User hit next with no history. Populating Data");
                    nextSong.name = mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    nextSong.subtitle = mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    nextSong.data = mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                    nextIntent.putExtra(ADD_TO_SONG_LIST, true);
                }
                else if ((mHistoryPos < songHistory.size()-1) && shuffleMode == SHUFFLE_NONE){
                    mHistoryPos++;
                    nextSong = songHistory.get(mHistoryPos);
                    nextIntent.putExtra(ADD_TO_SONG_LIST, false);
                }
                else{
                    int newPosition = goToNextPos();
                    mCursor.moveToPosition(newPosition);
                    nextSong.name = mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    nextSong.subtitle = mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                    nextSong.data = mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                    nextIntent.putExtra(ADD_TO_SONG_LIST, true);
                }
                nextIntent.putExtra(SONG, nextSong);
                nextIntent.putExtra(PLAY_NOW, true);
                nextIntent.putExtra(SET_IN_PLAYER, true);
                nextIntent.putExtra(CURRENT_PLAYLIST, mPlaylistSelection);
                nextIntent.putExtra(Constants.POSITION, mCursor.getPosition());
                bManager.sendBroadcast(nextIntent);
            } else if (action.equals(AudioService.ACTION_PREVIOUS)) {
                previous();
                Song previousSong = songHistory.get(mHistoryPos);
                Log.i(LOGTAG, "Set previous to song to play with index " + mHistoryPos);
                Intent previousIntent = new Intent(MusicPlayerFragment.ADD_SONG);
                previousIntent.putExtra(SONG, previousSong);
                previousIntent.putExtra(ADD_TO_PLAYING_NOW, false);
                previousIntent.putExtra(SET_IN_PLAYER, true);
                previousIntent.putExtra(ADD_TO_SONG_LIST, false);
                previousIntent.putExtra(Constants.POSITION, mPlayPos);
                previousIntent.putExtra(CURRENT_PLAYLIST, mPlaylistSelection);
                bManager.sendBroadcast(previousIntent);
            } else if (action.equals(MusicPlayerFragment.ADD_SONG)){
                Log.d(LOGTAG, "Received request to add song");
                Song newSong = intent.getParcelableExtra(SONG);
                boolean playNow = intent.getBooleanExtra(PLAY_NOW,false);
                boolean setInPlayer = intent.getBooleanExtra(SET_IN_PLAYER, true);
                boolean addToSongList = intent.getBooleanExtra(ADD_TO_SONG_LIST, true);
                boolean addToPlayingNow = intent.getBooleanExtra(ADD_TO_PLAYING_NOW, false);
                int currentPlaylist = intent.getIntExtra(CURRENT_PLAYLIST, PLAYLIST_SONG);
                int newPosition = intent.getIntExtra(Constants.POSITION,0);
                Log.d(LOGTAG,"Current position is "+ mPlayPos + " and the next position is " + newPosition);
                addToHistroy = addToSongList;
                setCurrentSong(newSong);
                if (playNow && ((songHistory.size()-1) == mHistoryPos)){
                    Log.d(LOGTAG,"Changing position to "+ newPosition);
                    mHandler.obtainMessage(POSITION_CHANGED,newPosition,0).sendToTarget();
                }

                if (mPlaylistSelection != currentPlaylist){
                    setCurrentPlaylist(currentPlaylist);
                    mHandler.obtainMessage(PLAYLIST_CHANGED).sendToTarget();
                }


                if (setInPlayer) {
                    Log.i(LOGTAG, "Playing song "+ newSong.name +" by "+ newSong.subtitle);
                    playSong(newSong.data,playNow);
                    isPlayingSong = true;
                    //buildNotification(newSong);
                }
            } else if (action.equals(AudioService.REMOVE_SONG)){
                // TODO need to rework remove song
                // TODO add dynamic playlist functionality to AudioSerivce
                int position = intent.getIntExtra(POSITION, NONE);
                if (position > 0 ){
                    //removeSong(position);
                }
            } else if (action.equals(AudioService.BOUND_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BOUND_STATE_CHANGED,BOUND_STATE_UNBOUND);
                mHandler.post( new Runnable() {
                    @Override
                    public void run() {
                        if (state == BOUND_STATE_UNBOUND){
                            setBound(false);
                        }
                        else{
                            setBound(true);
                        }

                    }
                });
            }

        }
    };

    private void setCurrentSong(Song newSong) {
        synchronized (this){
            mCurrentSong = newSong;
        }
    }

    private void setPlayPos(int position){
        synchronized (this){
            mPlayPos = position;
        }
    }

    private void setCurrentPlaylist(int currentPlaylist) {
        synchronized (this){
            mPlaylistSelection = currentPlaylist;
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
            // Pause playback
            pause();
        } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
            // Resume playback
            play();
        } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
            //TODO create broadcast receiver to listen to media button for control of app using hardware keys
            //aManager.unregisterMediaButtonEventReceiver(receiver);
            aManager.abandonAudioFocus(this);
            // Stop playback
            pause();
        }
    }

    public class LocalBinder extends Binder {
        public AudioService getService(){
            return AudioService.this;
        }
    }

    @Override
    public void onCreate() {
        // Initialize the position
        mPlayPos = 0;
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnCompletionListener(completionListener);
        mMediaPlayer.setOnPreparedListener(preparedListener);

        mMediaPlayer.setOnErrorListener(errorListener);

        notificationManager = NotificationManagerCompat.from(getApplicationContext());
        bManager = LocalBroadcastManager.getInstance(getApplicationContext());
        IntentFilter filter =  new IntentFilter();
        filter.addAction(AudioService.ACTION_PLAY);
        filter.addAction(AudioService.ACTION_PAUSE);
        filter.addAction(AudioService.ACTION_NEXT);
        filter.addAction(AudioService.ACTION_PREVIOUS);
        filter.addAction(AudioService.PLAY_STATE_CHANGED);
        filter.addAction(AudioService.BOUND_STATE_CHANGED);
        filter.addAction(AudioService.REMOVE_SONG);
        filter.addAction(MusicPlayerFragment.ADD_SONG);
        bManager.registerReceiver(receiver, filter);

        HandlerThread thread = new HandlerThread("Thread name", android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        Looper looper = thread.getLooper();
        mHandler = new ServiceHandler(looper);

        initMusicPlayer();
        new UpdateDefaultPlaylist().execute();
    }


    private class UpdateDefaultPlaylist extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            setCurrentPlaylist(PLAYLIST_SONG);
            Log.d(LOGTAG, "Initializing default playlist to songs");
            mHandler.obtainMessage(PLAYLIST_CHANGED).sendToTarget();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d(LOGTAG, "Finished with default playlist init");
        }
    }



    MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            // Send an intent to play the next song
            bManager.sendBroadcast(new Intent(AudioService.ACTION_NEXT));
        };
    };

    MediaPlayer.OnPreparedListener preparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mediaPlayer) {
            mIsInitialized = true;
            seekTo(0);
            try {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.start();
                Thread.sleep(500);
                shouldUpdate = true;
                new Thread(){
                    Intent intent;
                    // Constantly update the seekbar
                    @Override
                    public void run() {
                        Log.i(LOGTAG, "Mediaplayer prepared was called");
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (mMediaPlayer == null){
                            Log.e(LOGTAG, "Media player should not be null");
                        }
                        Log.d(LOGTAG,"Going to call get Duration from Service");
                        while(mMediaPlayer != null && getPosition() < getDuration()){
                            if (isPlaying() && isBound()){
                                if (shouldUpdate){
                                    intent = new Intent(NowPlayingActivity.UPDATE_SEEKBAR);

                                    intent.putExtra(CURRENT_POSITION, getPosition());
                                    intent.putExtra(DURATION, getDuration());
                                    bManager.sendBroadcast(intent);
                                }
                                else{
                                    Log.e(LOGTAG, "Media player is not ready yet");
                                    try {
                                        sleep(500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            try {
                                sleep(20);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }.start();

            } catch (SecurityException e1) {
                e1.printStackTrace();
            } catch (IllegalStateException e1) {
                e1.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            Intent intent = new Intent(BaseActivity.UPDATE_VIEW);
            intent.putExtra("duration", getDuration());
            intent.putExtra(Constants.NOW_PLAYING_TITLE, mCurrentSong.name);
            intent.putExtra(Constants.NOW_PLAYING_ARTIST, mCurrentSong.subtitle);
            intent.putExtra("data", mCurrentSong.data);
            bManager.sendBroadcast(intent);
            mHandler.post(new Runnable() {
                // Set the attributes and add to history if needed
                @Override
                public void run() {
                    synchronized (this){
                        if (addToHistroy){
                            setAttributes();
                            songHistory.add(mCurrentSong);
                            mHistoryPos++;
                        }
                    }
                }
            });
        }
    };

    private void setAttributes(){
        synchronized (this){
// TODO need to set the correct attributes i.e. if it comes from genere in the fragment - set title and subtitle from the textviews
            int id = mCursor.getInt(mCursor.getColumnIndex(MediaStore.Audio.Media._ID));
            mCurrentSong.id =id;
        }
    }

    MediaPlayer.OnErrorListener errorListener = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
            switch (what){
                case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                    mIsInitialized = false;
                    mMediaPlayer.release();
                    mMediaPlayer= null;
                    mediaPlayer = new MediaPlayer();
                    initMusicPlayer();
                    mHandler.sendMessageDelayed(mHandler.obtainMessage(SERVER_DIED), 2000);
                    return true;
                default:
                    Log.d(LOGTAG, "MediaPlayer error: " + what + "," + extra);
                    break;

            }
            return false;
        }
    };


    public void initMusicPlayer(){
        //set the player properties here
        synchronized (this) {
            mMediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }
    }


    public void buildNotification(Song nowPlaying){
        // TODO Finalize once verified
        Intent notIntent = new Intent(this, NowPlayingActivity.class);
        PendingIntent notifPending = PendingIntent.getActivity(
                getApplicationContext(), 0, notIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
                        | PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder noteB = new NotificationCompat.Builder(getApplicationContext());
        noteB.setContentTitle("Playing " + nowPlaying.name);
        noteB.setContentText(nowPlaying.subtitle);
        noteB.setSmallIcon(R.drawable.ic_action_name);
        noteB.setOngoing(true);
        noteB.setLargeIcon(
                BitmapFactory.decodeResource(getResources(), R.drawable.ic_music_icon)
        );
        noteB.setTicker("Playing song").setContentIntent(notifPending);
        Notification note = noteB.build();

        //notificationManager.notify(NOTIFICATION_ID,note);
        startForeground(NOTIFICATION_ID,note);
    }

    public boolean playSong(String uri, boolean playNow){

        if(mMediaPlayer != null && mMediaPlayer.isPlaying()){
            mMediaPlayer.pause();
        }
        Log.d(LOGTAG, "Play song was called");
        mMediaPlayer.reset();
        mMediaPlayer.release();
        mMediaPlayer = null;
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnPreparedListener(preparedListener);
        mMediaPlayer.setOnCompletionListener(completionListener);
        mMediaPlayer.setOnErrorListener(errorListener);

        initMusicPlayer();

        try {
            mMediaPlayer.setDataSource(uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mMediaPlayer.prepareAsync();
        //mMediaPlayer.setLooping(true);

        return true;
    }


    public void play(){
        aManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);

        requestFocusResult = aManager.requestAudioFocus(this,AudioManager.STREAM_MUSIC,AudioManager.AUDIOFOCUS_GAIN);

        if (requestFocusResult == AudioManager.AUDIOFOCUS_REQUEST_GRANTED){
            mMediaPlayer.start();
        }
    }

    public void pause(){
        mMediaPlayer.pause();
        //aManager.abandonAudioFocus(this);
    }

    public boolean isPlaying(){
        try {
            return this.mMediaPlayer.isPlaying();
        }
        catch (IllegalArgumentException e){
            return false;
        }

    }

    private static boolean shouldUpdate = false;
    private boolean sendUpdate(){
        return sendUpdate();
    }

    private void setUpdate(boolean update){
        shouldUpdate = update;
    }

    public void previous(){
        if (songHistory.size() <= 1 ){
            mHistoryPos = 0;
        }
        else{
            mHistoryPos = songHistory.size() -1;
        }
    }

    public int getNextPos() {
        if (repeatMode == REPEAT_CURRENT){
            if (mPlayPos < 0)
                return 0; // If you haven't played a song before, start from the beginning
            return mPlayPos;
        }
        else if (shuffleMode == SHUFFLE_NORMAL) {
            if (mPlayPos >= 0) { //If we are playing something
                shuffleHistory.add(mPlayPos);
            }
            // Keep Vector size to 1000 elements
            if (shuffleHistory.size() > 1000) {
                shuffleHistory.removeElementAt(0);
            }
            int numTracks = mCursor.getCount();

            int skipToPos;
            do {
                skipToPos = mShuffler.nextInt(numTracks);
            }while(shuffleHistory.contains(skipToPos)) ;
            return skipToPos;
        }
        if (mCursor.isLast()){
            if (repeatMode ==  REPEAT_ALL){
                return 0;
            } else if (repeatMode ==  REPEAT_NONE){
                return FINISHED;
            }
        }  else{
            return mPlayPos + 1;
        }
        return mPlayPos;
    }

    public void removeSong(int position){
        songHistory.remove(position);
        synchronized (this){
        */
/* if the song removed was in queue, change
           index to account for queue sizer change
         *//*

            if (mPlayPos >= position){
                mPlayPos--;
            }
        }
    }

    public long getDuration() {
        if (isInitialized() && mMediaPlayer != null){
            return mMediaPlayer.getDuration();
        }
        return 1000;
    }

    public long getPosition(){
        if (isInitialized() && mMediaPlayer != null){
            return mMediaPlayer.getCurrentPosition();

        }
        Log.d(LOGTAG, "Could not get Position");
        return NONE;
    }

    public long seekTo(long position){
        synchronized (this){
            if (isInitialized()){
                if (position < 0 ){
                    position = 0;
                }
                if (position > mMediaPlayer.getDuration()){
                    position = mMediaPlayer.getDuration();
                }
                mMediaPlayer.seekTo((int) position);
            }
            return FINISHED;
        }

    }

    public int goToNextPos() {
        int nextPos = getNextPos();
        Log.d(LOGTAG,"Going from position "+ mPlayPos +" to " + nextPos);
        if (mPlayPos < 0){
            Log.d(LOGTAG, "No song to go to, so returning to start");
            nextPos = 0;
        }
        return nextPos;
    }

    public String getTrackName(){
        synchronized (this){
            if (mCursor == null){
                return null;
            }
            return mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
        }
    }

    public String getArtistName(){
        synchronized (this){
            if (mCursor == null)
                return null;
            return mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
        }
    }

    public String getData(){
        synchronized (this){
            if (mCursor == null)
                return null;
            return mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.DATA));
        }
    }

    public int getShuffleMode(){
        return this.shuffleMode;
    }

    public int getRepeatMode(){
        return this.repeatMode;
    }


    private Vector<Integer> shuffleHistory = new Vector<>(1000); //starting capacity 1000
    private Shuffler mShuffler = new Shuffler();

    private static class  Shuffler {
        private int mPrevious;
        private Random mRandom = new Random();

        private int nextInt(int range){
            int result;
            do{
                result = mRandom.nextInt(range);
            }while (result == mPrevious && range > 1);
            mPrevious = result;
            return result;
        }
    }

    @Override
    public void onDestroy() {
        // Close the cursor as we will not need it
        mCursor.close();
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()){
                mMediaPlayer.stop();
                aManager.abandonAudioFocus(this);
            }
            mMediaPlayer.release();
        }
        mMediaPlayer = null; // Release the media resources
        bManager.unregisterReceiver(receiver);
        //notificationManager.cancel(NOTIFICATION_ID);
        stopForeground(true);
        // Save the song that was currently playing if you were playing a song
        if(songHistory.isEmpty()){
            Song lastSong = mCurrentSong;
            SharedPreferences settings = getSharedPreferences(PREFTAG,MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("song_id", lastSong.id);
            editor.putString("song_name", lastSong.name);
            editor.putString("song_artist", lastSong.subtitle);
            editor.putString("album_art", lastSong.albumArtUri);
            editor.putString("song_data", lastSong.data);
            editor.putInt("shuffle_mode", shuffleMode);
            editor.putInt("repeat_mode", repeatMode);
            editor.commit();
        }
        super.onDestroy();
    }


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    public boolean isBound() {
        return this.bound;
    }

    public void setBound(boolean bound) {
        synchronized (this){
            this.bound = bound;
            if (mMediaPlayer == null){
                stopSelf();
            }
        }

    }




    public boolean isInitialized(){
        return mIsInitialized;
    }

    public void refreshFragments(){
        Intent intent = new Intent(AudioService.REFRESH_ALL);
        intent.putExtra(DURATION, getDuration());
        intent.putExtra(SONG, mCurrentSong);
        intent.putExtra(Constants.CURRENT_PLAYLIST, mPlaylistSelection);
        intent.putExtra(REPEAT_MODE, repeatMode);
        intent.putExtra(SHUFFLE_MODE, shuffleMode);
        bManager.sendBroadcast(intent);
    }

    public Bundle refreshFragments(int context){
        Bundle args = new Bundle();
        switch (context) {
            case AudioService.REFRESH_ACTIVITY:
                args.putLong("duration", getDuration());
                return args;
            case AudioService.REFRESH_PLAYER_FRAGMENT:
                args.putParcelable("song", mCurrentSong);
                args.putInt("repeatMode", repeatMode);
                args.putInt("shuffleMode", shuffleMode);
                return args;
            case AudioService.REFRESH_NOW_PLAYING_FRAGMENT:
                args.putParcelableArrayList("history", songHistory);
                return args;
        }
        return null;
    }

    private static final MyHandler aHandler = new MyHandler();

    private static class MyHandler extends Handler{
        static float mCurrentVolume = 1.0f;
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case FADE_DOWN:
                    mCurrentVolume -= .05f;
                    if (mCurrentVolume > .2f) {
                        aHandler.sendEmptyMessageDelayed(FADE_DOWN, 10);
                    } else {
                        mCurrentVolume = .2f;
                    }
                    mMediaPlayer.setVolume(mCurrentVolume,mCurrentVolume);
                    break;
                case FADE_UP:
                    mCurrentVolume += .01f;
                    if (mCurrentVolume < 1.0f) {
                        aHandler.sendEmptyMessageDelayed(FADE_UP, 10);
                    } else {
                        mCurrentVolume = 1.0f;
                    }
                    mMediaPlayer.setVolume(mCurrentVolume,mCurrentVolume);
                    break;
            }
        }
    }

    static class ServiceStub extends IMediaPlaybackService.Stub {
        WeakReference<AudioService> mService;

        ServiceStub(AudioService service) {
            mService = new WeakReference<AudioService>(service);
        }

        public boolean isPlaying() {
            return mService.get().isPlaying();
        }

        public long getDuration() {
            return mService.get().getDuration();
        }

        public long seekTo(long position) {
            return mService.get().seekTo(position);
        }

    }

    private final IBinder mBinder = new ServiceStub(this);

}

// TODO - Handle dealing with audio output hardware*/
