// IMediaPlaybackService.aidl
package com.ctech.music.androidstreamer;

// Declare any non-default types here with import statements

interface IMediaPlaybackService {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void openFile(String path);
    void open(in long [] list, int position);
    int getQueuePosition();
    void stop();
    void pause();
    void play();
    void prev();
    void next();
    long getDuration();
    boolean isPlaying();
    long seekTo(long position);
    String getTrackName();
    String getAlbumName();
    long getAlbumId();
    String getArtistName();
    long getArtistId();
    String getPath();
    void enqueue(in long [] list, int action);
    long [] getQueue();
    void moveQueueItem(int from, int to);
    void setQueuePosition(int index);
    long getAudioId();
    void setShuffleMode(int shuffleMode);
    int getShuffleMode();
    int removeTracks(int first, int last);
    int removeTrack(long id);
    void setRepeatMode(int repeatMode);
    int getRepeatMode();
    int getMediaMountedCount();
    int getAudioSessionId();
}
