package com.ctech.music.androidstreamer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;

/**
 * Created by Charles on 25/03/2015.
 */
public class MediaButtonReceiver extends BroadcastReceiver {

    private static final int MSG_LONGPRESS_TIMEOUT = 1;
    private static final int LONG_PRESS_DELAY = 1000;

    private static long mLastClickTime = 0;
    private static boolean mDown = false;
    private static boolean mLaunched = false;

    private final static MyHandler mHandler = new MyHandler();

    private static class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_LONGPRESS_TIMEOUT:
                    if (!mLaunched) {
                        Context context = (Context)msg.obj;
                        Intent i = new Intent();
                        i.setClass(context, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(i);
                        mLaunched = true;
                    }
                    break;
            }
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
            Intent i = new Intent(context, AudioService.class);
            i.setAction(AudioService.SERVICECMD);
            i.putExtra(AudioService.ACTION, AudioService.ACTION_PAUSE);
            context.startService(i);
        }
        else if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
            KeyEvent event = (KeyEvent)intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);

            if (event == null)
                return;
            int keyCode = event.getKeyCode();
            //TODO Complete broadcast reciver
            String action = null;
            switch (keyCode) {
                case KeyEvent.KEYCODE_MEDIA_STOP:
                    action = AudioService.ACTION_STOP;
                    break;
                case KeyEvent.KEYCODE_HEADSETHOOK:
                case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                    action = AudioService.ACTION_PAUSE;
                    break;
                case KeyEvent.KEYCODE_MEDIA_NEXT:
                    action = AudioService.ACTION_NEXT;
                    break;
                case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                    action = AudioService.ACTION_PREVIOUS;
                    break;
                case KeyEvent.KEYCODE_MEDIA_PAUSE:
                    action = AudioService.ACTION_PAUSE;
                    break;
                case KeyEvent.KEYCODE_MEDIA_PLAY:
                    action = AudioService.ACTION_PLAY;
                    break;
            }
            // If none of the media keys were pessed
            if (action != null){
               //TODO Implment
            }
        }
    }
}


/*
* // Start listening for button presses
am.registerMediaButtonEventReceiver(MediaButtonReceiver);
...

// Stop listening for button presses
am.unregisterMediaButtonEventReceiver(MediaButtonReceiver);
*
 */