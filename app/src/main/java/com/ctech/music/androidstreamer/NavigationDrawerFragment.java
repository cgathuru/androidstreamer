package com.ctech.music.androidstreamer;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {

    public static final String PREF_FILE_NAME = "testpref";
    public static final String KEY_USER_LEARNED_DRAWER="user_learned_drawer";
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View containerView;
    private RecyclerView recyclerView;
    private DrawerItemAdapter adapter;
    private boolean mUserLearnedDrawer;
    private boolean mFromSavedInstanceState;
    private FragmentManager manager;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLearnedDrawer = Boolean.valueOf(readFromSharedPreferences(getActivity(),KEY_USER_LEARNED_DRAWER,"false"));

        if(savedInstanceState != null){
            //Coming back from rotation
            mFromSavedInstanceState = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        recyclerView =(RecyclerView) layout.findViewById(R.id.drawer_list);
        adapter = new DrawerItemAdapter(getActivity(),getData());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),recyclerView) {
            @Override
            public void onClick(View view, int position) {
                FragmentTransaction transaction = manager.beginTransaction();
                switch (position){
                    case 0:
                        transaction.replace(R.id.fragment_container, new LibraryFragment());
                        break;
                    case 1:
                        transaction.replace(R.id.fragment_container, new PlaylistsFragment());
                        break;
                    case 2:
                        transaction.replace(R.id.fragment_container, new InternetRadioFragment());
                        break;
                    default:
                        transaction.replace(R.id.fragment_container, new LibraryFragment());
                        break;
                }

                transaction.commit();
                closeDrawer();

            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(getActivity(),"onLongClick " + position,Toast.LENGTH_SHORT).show();
            }
        });

        return layout;
    }


    public void closeDrawer() {
        mDrawerLayout.closeDrawer(containerView);
    }

    public void openDrawer(){
        mDrawerLayout.openDrawer(containerView);
    }


    public List<DrawerItemData> getData(){
        List<DrawerItemData> data = new ArrayList<>();
        int[] icons = {R.drawable.ic_action_name,R.drawable.ic_action_name,R.drawable.ic_action_name};
        String[] titles = getResources().getStringArray(R.array.nav_drawer_items);
        for (int i=0; i< titles.length; i++){
            DrawerItemData info = new DrawerItemData();
            info.setTitle(titles[i]);
            info.setIconPath(icons[i]);
            data.add(info);
        }
        return data;
    }

    public void setUp(int fragmentId, final DrawerLayout drawerLayout, final Toolbar toolbar, final FragmentManager manager) {
        this.manager = manager;
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(),drawerLayout,toolbar,R.string.drawer_open,R.string.drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                if(!mUserLearnedDrawer){
                    // User has not seen drawer before
                    mUserLearnedDrawer = true;
                    saveToSharedPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, mUserLearnedDrawer+"");
                }
                getActivity().invalidateOptionsMenu();
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                getActivity().invalidateOptionsMenu();
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (slideOffset <0.6){
                    toolbar.setAlpha(1-slideOffset);
                }

            }
        };
        if (!mUserLearnedDrawer && !mFromSavedInstanceState){
            mDrawerLayout.openDrawer(containerView);
        }
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
        closeDrawer();
    }

    public static void saveToSharedPreferences(Context context, String preferenceName, String preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public static String readFromSharedPreferences(Context context, String preferenceName, String preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName,preferenceValue);
    }


}
