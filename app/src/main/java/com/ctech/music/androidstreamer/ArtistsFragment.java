package com.ctech.music.androidstreamer;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by Charles on 19/03/2015.
 */
public class ArtistsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    private RecyclerView mrecyclerView;
    private ArtistCursorRecycleAdapter madapter;
    private LocalBroadcastManager bManager;
    private Cursor mCursor;

    private static final String LOGTAG = "SongsFragment";
    private static final Uri uri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;
    private static final String selection = null;
    private static final String order = MediaStore.Audio.Artists.ARTIST + " ASC";
    private static final String[] projection = {
            MediaStore.Audio.Artists._ID, MediaStore.Audio.Artists.ARTIST,
            MediaStore.Audio.Artists.NUMBER_OF_ALBUMS
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout =  inflater.inflate(R.layout.fragment_library_artists,container,false);
        mrecyclerView = (RecyclerView) layout.findViewById(R.id.library_page_artists);
        madapter = new ArtistCursorRecycleAdapter(getActivity(),null);
        mrecyclerView.setAdapter(madapter);
        getLoaderManager().initLoader(0, null, this);
        mrecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mrecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mrecyclerView) {
            @Override
            public void onClick(View view, int position) {
                Intent nextIntent = new Intent(MusicPlayerFragment.ADD_SONG);
                Song nextSong = new Song();
                int currentPos = mCursor.getPosition();
                mCursor.moveToPosition(position);
               /* nextSong.name = mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                nextSong.subtitle = mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                nextSong.data = mCursor.getString(mCursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                nextIntent.putExtra(Constants.SONG, nextSong);
                nextIntent.putExtra(Constants.PLAY_NOW, true);
                nextIntent.putExtra(Constants.ADD_TO_PLAYING_NOW, true);
                nextIntent.putExtra(Constants.ADD_TO_SONG_LIST,true);
                nextIntent.putExtra(Constants.SET_IN_PLAYER, true);
                nextIntent.putExtra(Constants.POSITION,position);
                nextIntent.putExtra(Constants.CURRENT_PLAYLIST, AudioService.PLAYLIST_SONG);
                bManager.sendBroadcast(nextIntent);*/

            }

            @Override
            public void onLongClick(View view, int position) {
                // TODO Create options menu
            }
        });


        bManager = LocalBroadcastManager.getInstance(getActivity());
        return layout;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        CursorLoader loader = new CursorLoader(getActivity(),uri,projection,selection,null,order);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(LOGTAG,"Swapping the cursor");
        madapter.swapCursor(data);
        mCursor = data;

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        madapter.swapCursor(null);
        mCursor = null;

    }

    //TODO Handle click events




}
