package com.ctech.music.androidstreamer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

//import com.ctech.music.androidstreamer.OldService.LocalBinder;


public class MainActivity extends BaseActivity {

    private Toolbar toolbar;
    private FragmentManager fragmentManager;

    private Utilities.ServiceToken mToken;
    private static boolean isBound= false;
    private Intent playIntent;

    // TODO set audio service song list

    /*private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocalBinder binder = (LocalBinder) service;
            audioService = binder.getService();
            isBound = true;
            audioService.setBound(true);
            resetPausePlayButton();
            duration.setText(Utilities.makeTimeString(audioService.getDuration()));
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound=false;
        }
    };
*/
    private BroadcastReceiver reciever= new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // TODO
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_appbar);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fragmentManager = getSupportFragmentManager();
        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment)
                fragmentManager.findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout)findViewById(R.id.drawer_layout), toolbar, fragmentManager);
        mToken = Utilities.bindToService(this, this);
        addLibrary();

    }


    @Override
    protected void onResume() {
        super.onResume();

        // Make sure that the home button is clickable as up
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onArtClick(View v) {
        startActivity(new Intent(this,NowPlayingActivity.class));
    }

    public void addLibrary(){
        LibraryFragment libraryFragment = new LibraryFragment();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container,libraryFragment,"library");
        transaction.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Utilities.unbindFromService(mToken);
    }
}
