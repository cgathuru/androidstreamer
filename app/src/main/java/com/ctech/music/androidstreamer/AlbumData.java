package com.ctech.music.androidstreamer;

/**
 * Created by Charles on 20/03/2015.
 */
public class AlbumData extends DrawerItemData{
    private int noAlbums;

    public void setNoAlbums(int noAlbums){this.noAlbums = noAlbums;}

    public int getNoAlbums() {return this.noAlbums;}
}
