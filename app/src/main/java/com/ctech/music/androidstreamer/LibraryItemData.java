package com.ctech.music.androidstreamer;

/**
 * Created by Charles on 20/03/2015.
 */
public class LibraryItemData {

    protected String iconPath;
    protected String title;
    protected String subtitle;
    protected long id;

    public void setId(long id){this.id = id;}

    public void setTitle(String title){
        this.title = title;
    }

    public void setIconPath(String iconPath){
        this.iconPath = iconPath;
    }

    public void setSubtitle(String subtitle){
        this.subtitle = subtitle;
    }

    public String getTitle(){
        return this.title;
    }

    public String getIconPath(){
        return this.iconPath;
    }

    public String getSubtitle(){ return this.subtitle; }

    public long getId(){return this.id;}

}
