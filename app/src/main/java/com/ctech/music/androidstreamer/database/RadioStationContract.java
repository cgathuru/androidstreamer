package com.ctech.music.androidstreamer.database;

import android.provider.BaseColumns;

/**
 * Created by EGATCHA on 3/24/2015.
 */


public final class RadioStationContract {

    public static abstract class RadioStationEntry implements BaseColumns{

        public static final String TABLE_NAME = "radio_station";
        public static final String COLUMN_NAME_RADIO_STATION_ID = "radio_station_id";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_SUBTITLE = "subtitle";
        public static final String COLUMN_NAME_GENRE = "genre";
        public static final String COLUMN_NAME_BITRATE= "bitrate";
        public static final String COLUMN_NAME_URI = "uri";

    }

}
