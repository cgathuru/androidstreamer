package com.ctech.music.androidstreamer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by Charles on 20/03/2015.
 */
public class SongAdapter extends RecyclerView.Adapter<SongAdapter.MyViewHolder>{

    private LayoutInflater inflater;
    private Context context;
    List<Song> data = Collections.emptyList();
    BitmapManager bitmapManager;

    public SongAdapter(Context context, List<Song> data){
        this.context = context;
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        bitmapManager = new BitmapManager(context);
    }

    @Override
    public SongAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.library_songs_item, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(SongAdapter.MyViewHolder holder, int position) {
        Song current = data.get(position);
        holder.songName.setText(current.name);
        holder.artistName.setText(current.artist);
        holder.icon.setImageBitmap(bitmapManager.fetchDrawable(Long.valueOf(current.albumId)));
        //Drawable img = Drawable.createFromPath(current.getIconPath());
        //holder.icon.setImageDrawable(img);
        //holder.nowPlaying.setImageResource(current.getNowPlayingIconId());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView songName;
        TextView artistName;
        ImageView icon;
        ImageView nowPlaying;

        public MyViewHolder(View itemView) {
            super(itemView);
            songName = (TextView) itemView.findViewById(R.id.item_title);
            artistName = (TextView) itemView.findViewById(R.id.item_subtitle);
            icon = (ImageView) itemView.findViewById(R.id.item_album_art);
            nowPlaying =(ImageView) itemView.findViewById(R.id.item_playing_icon);
        }
    }

}