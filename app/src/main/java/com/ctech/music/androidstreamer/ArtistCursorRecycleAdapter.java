package com.ctech.music.androidstreamer;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio.Media;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;

import wseemann.media.FFmpegMediaMetadataRetriever;

/**
 * Created by EGATCHA on 3/18/2015.
 * This Custom Adapter is for Songs
 */
 class ArtistCursorRecycleAdapter extends CursorRecycleAdapter<SongHolder> {

    private static final String LOGTAG = "ArtistRecycleAdapter";
    private Context mContext;

    public ArtistCursorRecycleAdapter(Context context, Cursor cursor) {
        super(cursor);
        this.mContext = context;
    }

    @Override
    public void onBindViewHolderCursor(SongHolder holder, Cursor cursor) {

        String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Artists.ARTIST));
        String subtitle = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Artists.NUMBER_OF_ALBUMS));
        // Make sure the cursor has valid data

       // long albumId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Artists.));
        //Uri songCover = Uri.parse("content://media/external/audio/albumart");
        //Uri uriSongCover = ContentUris.withAppendedId(songCover, albumId);
       // Picasso.with(mContext).load(uriSongCover).fit().into(holder.albumArt);
        holder.title.setText(title);
        holder.subtitle.setText(subtitle);


    }

    @Override
    public SongHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.library_artists_item,parent,false);
        return new SongHolder(view);
    }
}