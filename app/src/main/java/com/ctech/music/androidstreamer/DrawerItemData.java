package com.ctech.music.androidstreamer;

/**
 * Created by Charles on 17/03/2015.
 */
public class DrawerItemData {
    private int iconPath;
    private String title;



    public void setTitle(String title){
        this.title = title;
    }

    public void setIconPath(int iconPath){
        this.iconPath = iconPath;
    }

    public String getTitle(){
        return this.title;
    }

    public int getIconPath(){
        return this.iconPath;
    }
}
