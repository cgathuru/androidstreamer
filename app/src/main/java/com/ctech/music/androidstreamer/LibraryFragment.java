package com.ctech.music.androidstreamer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Charles on 18/03/2015.
 */
public class LibraryFragment extends Fragment {

    private ViewPager viewPager;

    public LibraryFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_library, container, false);
        viewPager = (ViewPager) layout.findViewById(R.id.library_pager);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        LibraryPagerAdapter pagerAdapter = new LibraryPagerAdapter(fragmentManager);
        pagerAdapter.setTitles(getResources().getStringArray(R.array.library_titles));
        viewPager.setAdapter(pagerAdapter);

        return layout;
    }

}

class LibraryPagerAdapter extends FragmentStatePagerAdapter{

    String[] titles;

    public LibraryPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setTitles(String[] libraryTitles){
        titles = libraryTitles;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position){
            case 0:
                fragment = new SongsFragment();
                break;
            case 1:
                fragment = new ArtistsFragment();
                break;
            case 2:
                fragment = new AlbumsFragment();
                break;
            case 3:
                fragment = new GenresFragment();
                break;
            case 4:
                fragment = new YearFragment();
                break;
            default:
                fragment = null;
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}

