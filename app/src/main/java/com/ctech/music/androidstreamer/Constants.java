package com.ctech.music.androidstreamer;

/**
 * Created by Charles on 22/03/2015.
 */
public final class Constants {

    public static final String SONG_NAME = "song_name";
    public static final String SONG = "song";
    public static final String POSITION = "position";
    public static final String REPEAT_MODE = "repeatMode";
    public static final String SHUFFLE_MODE = "shuffleMode";
    public static final String ADD_TO_PLAYING_NOW  = "addToPlayingNow";
    public static final String ADD_TO_SONG_LIST = "addToSongList";
    public static final String PLAY_NOW = "playNow";
    public static final String SET_IN_PLAYER = "setInPLayer";
    public static final String DURATION = "duration";
    public static final String CURRENT_POSITION = "currentPosition";
    public static final String SONG_LIST = "songList";
    public static final String CURRENT_PLAYLIST = "currentPlaylist";
    public static final String NOW_PLAYING_TITLE = "title";
    public static final String NOW_PLAYING_ARTIST = "subtitle";
    public static final String NOW_PLAYING_BITRATE = "bitrate";
    public static final String NOW_PLAYING_ALBUM_ART = "art";
    public static final String IS_BOUND = "is_bound";
    public static final String DATA = "data";


}
